package ru.verna.alfabank;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import ru.verna.commons.Utils;
import ru.verna.commons.http.CommonHttpClient;
import ru.verna.commons.http.DescribingRequestWrapper;
import ru.verna.commons.log.Log;
import ru.verna.commons.mail.CommonMailClient;

/**
 * Методы-утилиты для работы с Альфа-банком.
 */
final class AlfaBankUtils {

    /**
     * Закрытый конструктор.
     */
    private AlfaBankUtils() {
    }

    /**
     * Метод по отправке запроса на отмену оплаты заказа.
     *
     * @param calcId          котировки
     * @param alfabankConfigs параметры подключения
     * @param alfaParams      параметры для передачи банку
     * @return строковое представление ответа банка на запрос отмены оплаты заказа
     */
    static String reverseOrder(String calcId, Map<String, String> alfabankConfigs, Map<String, String> alfaParams) {
        Logger mainLogger = Log.getLogger("main");
        String alfaServer = alfabankConfigs.get("baseurl") + alfabankConfigs.get("reverseOrder");
        String orderId = alfaParams.get("orderId");
        String result = "";
        try {
            mainLogger.info("Отправляем запрос на отмену оплаты заказа: [{}], orderId: [{}]", calcId, orderId);
            String acquirerResponse = CommonHttpClient.send(HttpGet.METHOD_NAME, alfaServer, alfaParams, null);
            mainLogger.info("Получен ответ на запрос по отмене оплаты котировки: [{}], ответ: [{}]", calcId,
                    acquirerResponse);
            result = acquirerResponse;
        } catch (Exception e) {
            mainLogger.error("Ошибка при попытке отмены оплаты заказа: [" + calcId + "], orderId: [" + orderId + "]", e);
        }
        return result;
    }

    /**
     * Создадим из ответа банка на запрос отмены оплаты договора объект {@link AlfaBankResponse}.
     *
     * @param reverseOrder полученный ответ банка
     * @return объект {@link AlfaBankResponse}, содержащий данные о коде и сообщении банка, или если не получилось
     * распарсить ответ, то описание ошибки с кодом -1
     */
    static AlfaBankResponse wrapReverseOrderResponse(String reverseOrder) {
        AlfaBankResponse result;
        try {
            JSONObject reverseOrderJSON = new JSONObject(reverseOrder);
            String msg = String.valueOf(reverseOrderJSON.get("errorMessage"));
            int code = Integer.parseInt(String.valueOf(reverseOrderJSON.get("errorCode")));
            result = new AlfaBankResponse(code, msg);
        } catch (JSONException e) {
            String msg = "Ошибка при попытке обработки ответа по отмене заказа: [" + reverseOrder + "]";
            Log.getLogger("main").error(msg, e);
            result = new AlfaBankResponse(-1, msg);
        }
        return result;
    }

    /**
     * Отправка сообщения электронной почтой адресатам, заданным в настройках приложения.
     *
     * @param request         входящий запрос
     * @param userMsg         отправленное пользователю сообщение
     * @param kiasResponse    ответ КИАСа
     * @param reverseResponse ответ банка на отмену оплаты заказа
     * @param ticket          номер инцидента
     */
    static void sendWarningMail(HttpServletRequest request,
                                String userMsg,
                                String kiasResponse,
                                AlfaBankResponse reverseResponse,
                                long ticket) {
        String subject = "Зарегистрирован инцидент №" + ticket;
        String mailMsg = generateMailMsg(request, userMsg, kiasResponse, reverseResponse);
        CommonMailClient.sendMail(subject, mailMsg);
    }

    /**
     * Формируем из переданных данных текст сообщения электронной почты.
     *
     * @param request         входящий запрос
     * @param userMsg         отправленное пользователю сообщение
     * @param kiasResponse    ответ КИАСа
     * @param reverseResponse ответ банка на отмену оплаты заказа
     * @return сформированный текст сообщения
     */
    private static String generateMailMsg(HttpServletRequest request,
                                          String userMsg,
                                          String kiasResponse,
                                          AlfaBankResponse reverseResponse) {
        String requestDescr;
        if (request instanceof DescribingRequestWrapper) {
            requestDescr = ((DescribingRequestWrapper) request).getDescription();
        } else {
            DescribingRequestWrapper describingRequestWrapper = new DescribingRequestWrapper(request);
            requestDescr = describingRequestWrapper.getDescription();
        }

        String delimiter = "\n";
        String doubleDelimiter = delimiter + delimiter;
        StringBuilder result = new StringBuilder();
        result.append("Сообщение, отправленное пользователю:").append(delimiter);
        result.append(userMsg).append(doubleDelimiter);
        result.append("Данные по инциденту:").append(delimiter);
        result.append("Входящий запрос: ").append(requestDescr).append(doubleDelimiter);
        result.append("Результат: ").append(kiasResponse).append(doubleDelimiter);
        result.append("Отмена оплаты: ").append(reverseResponse.toString()).append(doubleDelimiter);
        result.append("Дата инцидента: ").append(LocalDateTime.now().format(Utils.DATE_TIME_FORMATTER));
        return result.toString();
    }

    /**
     * Извещаем партнера о проведении платежа.
     *
     * @param partnerCallbackURL url партнера, на который будет отправлен callback-запрос.
     *                           Все вхождения подстроки {@code {summ}} будут заменены на значение суммы оплаты.
     * @param amount             сумма оплаты
     */
    static void noticePartner(String partnerCallbackURL, long amount) {
        Logger mainLogger = Log.getLogger("main");
        try {
            // сумма в рублях
            String amountInCurrency = BigDecimal.valueOf(amount).divide(BigDecimal.valueOf(100L))
                    .setScale(2).toString().replace(".", ",");
            partnerCallbackURL = partnerCallbackURL.replace("{summ}", amountInCurrency);
            mainLogger.info("Отправка коллбэка партнеру: partnerCallbackUrl:[" + partnerCallbackURL + "]");
            CommonHttpClient.send(HttpGet.METHOD_NAME, partnerCallbackURL, null, null);
        } catch (Exception e) {
            mainLogger.error("Ошибка при отправке коллбэка партнеру: partnerCallbackUrl:[" + partnerCallbackURL + "]", e);
        }
    }
}
