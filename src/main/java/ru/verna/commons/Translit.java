package ru.verna.commons;

import java.util.HashMap;
import java.util.Map;

/**
 * Преобразователь кириллического текста в транслитерацию.
 *
 */
public final class Translit {
    private static final Map<String, String> LETTERS = new HashMap<String, String>();

    static {
        LETTERS.put("А", "A");
        LETTERS.put("Б", "B");
        LETTERS.put("В", "V");
        LETTERS.put("Г", "G");
        LETTERS.put("Д", "D");
        LETTERS.put("Е", "E");
        LETTERS.put("Ё", "E");
        LETTERS.put("Ж", "Zh");
        LETTERS.put("З", "Z");
        LETTERS.put("И", "I");
        LETTERS.put("Й", "I");
        LETTERS.put("К", "K");
        LETTERS.put("Л", "L");
        LETTERS.put("М", "M");
        LETTERS.put("Н", "N");
        LETTERS.put("О", "O");
        LETTERS.put("П", "P");
        LETTERS.put("Р", "R");
        LETTERS.put("С", "S");
        LETTERS.put("Т", "T");
        LETTERS.put("У", "U");
        LETTERS.put("Ф", "F");
        LETTERS.put("Х", "X");
        LETTERS.put("Ц", "C");
        LETTERS.put("Ч", "Ch");
        LETTERS.put("Ш", "Sh");
        LETTERS.put("Щ", "Sch");
        LETTERS.put("Ъ", "'");
        LETTERS.put("Ы", "Y");
        LETTERS.put("Ъ", "'");
        LETTERS.put("Э", "E");
        LETTERS.put("Ю", "Yu");
        LETTERS.put("Я", "Ya");
        LETTERS.put("а", "a");
        LETTERS.put("б", "b");
        LETTERS.put("в", "v");
        LETTERS.put("г", "g");
        LETTERS.put("д", "d");
        LETTERS.put("е", "e");
        LETTERS.put("ё", "e");
        LETTERS.put("ж", "zh");
        LETTERS.put("з", "z");
        LETTERS.put("и", "i");
        LETTERS.put("й", "i");
        LETTERS.put("к", "k");
        LETTERS.put("л", "l");
        LETTERS.put("м", "m");
        LETTERS.put("н", "n");
        LETTERS.put("о", "o");
        LETTERS.put("п", "p");
        LETTERS.put("р", "r");
        LETTERS.put("с", "s");
        LETTERS.put("т", "t");
        LETTERS.put("у", "u");
        LETTERS.put("ф", "f");
        LETTERS.put("х", "x");
        LETTERS.put("ц", "c");
        LETTERS.put("ч", "ch");
        LETTERS.put("ш", "sh");
        LETTERS.put("щ", "sch");
        LETTERS.put("ъ", "'");
        LETTERS.put("ы", "y");
        LETTERS.put("ъ", "'");
        LETTERS.put("э", "e");
        LETTERS.put("ю", "yu");
        LETTERS.put("я", "ya");
    }

    /**
     * Закрытый конструктор.
     */
    private Translit() {
    }

    /**
     * Преобразовывает текст в транслитерированный.
     *
     * @param text транслитерируемая строка
     * @return транслитерацию переданной строки
     */
    public static String toTranslit(String text) {
        StringBuilder sb = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); i++) {
            String l = text.substring(i, i + 1);
            if (LETTERS.containsKey(l)) {
                sb.append(LETTERS.get(l));
            } else {
                sb.append(l);
            }
        }
        return sb.toString();
    }

}
