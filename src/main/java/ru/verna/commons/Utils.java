package ru.verna.commons;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.LinkedCaseInsensitiveMap;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

/**
 * Полезные методы общего назначения.
 */
public final class Utils {

    public static final Properties APP_PROPERTIES = new Properties();
    /**
     * Форматер для LocalDate.
     */
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    public static final Properties LOG_PROPERTIES = new Properties();
    private static Random random;

    static {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream logResourceAsStream = contextClassLoader.getResourceAsStream("log.properties");
                InputStream appResourceAsStream = contextClassLoader.getResourceAsStream("application.properties")) {
            LOG_PROPERTIES.load(logResourceAsStream);
            APP_PROPERTIES.load(appResourceAsStream);
        } catch (IOException e) {
            Log.getLogger("main").error(null, e);
        }
    }

    /**
     * Закрытый конструктор.
     */
    private Utils() {
    }

    /**
     * Возвращает <code>true</code>, если аргумент не null и его длина больше нуля, иначе <code>false</code>.
     *
     * @param val последовательность символов
     * @return <code>true</code>, если аргумент не null и его длина больше нуля, иначе <code>false</code>.
     */
    public static boolean notEmpty(CharSequence val) {
        return val != null && val.length() > 0;
    }

    /**
     * Возвращает <code>true</code>, если аргумент равен null или его длина равна нулю, иначе <code>false</code>.
     *
     * @param val последовательность символов
     * @return <code>true</code>, если аргумент равен null или его длина равна нулю, иначе <code>false</code>.
     */
    public static boolean isEmpty(CharSequence val) {
        return !notEmpty(val);
    }

    /**
     * Собирает строку с параметрами из переданной коллекции (param1=value1&param2=value2&#46;&#46;&#46;).
     *
     * @param parameters коллекция параметров
     * @return строку с параметрами из переданной коллекции (param1=value1&param2=value2&#46;&#46;&#46;).
     */
    public static String joinParameters(Map<String, String> parameters) {
        String paramsString = "";
        if (parameters != null) {
            List<NameValuePair> formparams = new ArrayList<>();
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            paramsString = URLEncodedUtils.format(formparams, "UTF-8");
        }
        return paramsString;
    }

    /**
     * Генерирует md5-хэш строки input.
     *
     * @param input входная строка
     * @return md5-хэш строки
     */
    public static String md5(String input) {
        String md5 = null;
        if (input == null) {
            return null;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(), 0, input.length());
            // CHECKSTYLE:OFF
            md5 = new BigInteger(1, digest.digest()).toString(16);
            // CHECKSTYLE:ON
        } catch (NoSuchAlgorithmException e) {
            Log.getLogger("main").error(null, e);
        }
        return md5;
    }

    /**
     * Генерирует SHA512-хэш строки input.
     *
     * @param input входная строка
     * @return SHA512-хэш строки
     */
    public static String sha512(String input) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            Log.getLogger("main").error(null, e);
        }
        byte[] digest = messageDigest != null ? messageDigest.digest(input.getBytes(StandardCharsets.UTF_8)) : new byte[0];
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    /**
     * Возвращает случайное число.
     *
     * @param scale размерность (длина) возвращаемого числа
     * @return случайное число заданной размерностью (по умолчанию 9)
     */
    public static long getRandomLong(int... scale) {
        // CHECKSTYLE:OFF
        int _scale;
        if (scale != null && scale.length > 0) {
            _scale = scale[0];
        } else {
            _scale = 9;
        }
        long shift = (long) Math.pow(10, _scale - 1);
        long base = (long) Math.pow(10, _scale) - shift - 1;
        long txNumber = shift + Math.round(Math.random() * base);
        return txNumber;
        // CHECKSTYLE:ON
    }

    /**
     * Генерирует случайное число в шестнадцатеричном формате.
     *
     * @return строку длиной 4 символа - случайное число в шестнадцатеричном формате.
     */
    public static String getSalt() {
        // CHECKSTYLE:OFF
        String salt = Integer.toHexString(4096 + getRandom().nextInt(36864));
        // CHECKSTYLE:ON
        return salt;
    }

    /**
     * @return the random
     */
    private static Random getRandom() {
        if (random == null) {
            random = new Random();
        }
        return random;
    }

    /**
     * Генерирует md5-хэш строки input, посоленную случайными символами. Саму соль добавляет в начало хэша.
     *
     * @param input хэшируемая строка
     * @return md5-хэш строки input, посоленную случайными символами. Саму соль добавляет в начало хэша.
     */
    public static String getSaltedMD5(String input) {
        String salt = getSalt();
        String md5 = md5(salt + input);
        return salt + md5;
    }

    /**
     * Возвращает стек вызовов в виде строки.
     *
     * @param t ошибка/исключение
     * @return стек вызовов в виде строки.
     */
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (IOException ex) {
        }
        return stackTrace;
    }

    /**
     * Возвращает часть строки:
     * <ul>
     * <li>Если {@code restriction} отсутствует или равен 0, то возвращает исходную строку;</li>
     * <li>Если {@code restriction} больше 0, то возвращает первые {@code restriction} символов строки;</li>
     * <li>Если {@code restriction} меньше 0, то возвращает последние {@code restriction} символов строки.</li>
     * </ul>
     *
     * @param str строка
     * @param restriction ограничение
     * @return ограниченную или нет строку
     */
    public static String restrictString(String str, int... restriction) {
        if (str == null) {
            return null;
        }
        String result = str;
        if (restriction.length > 0) {
            int strLength = str.length();
            int _restriction = restriction[0];
            final String threeDots = "...";
            if (_restriction > 0) {
                // возвращает первые Х символов строки
                result = str.substring(0, Math.min(strLength, _restriction));
                if (result.length() < str.length()) {
                    result = result + threeDots;
                }
            } else if (_restriction < 0) {
                // возвращает последние Х символов строки
                result = str.substring(Math.max(0, strLength + _restriction), strLength);
                if (result.length() < str.length()) {
                    result = threeDots + result;
                }

            }
        }
        return result;
    }

    /**
     * Создает стандартную xml-строку с описанием ошибки.
     *
     * @param errorCode код ошибки
     * @param errorMessage описание ошибки
     * @param parameter имя параметра (опционально)
     * @return стандартную xml-строку с описанием ошибки.
     */
    public static String getErrorMessage(int errorCode, String errorMessage, String... parameter) {
        return getErrorMessage(((Integer) errorCode).toString(), errorMessage, parameter);
    }

    /**
     * Создает стандартную xml-строку с описанием ошибки.
     *
     * @param errorCode код ошибки
     * @param errorMessage описание ошибки
     * @param parameter имя параметра (опционально)
     * @return стандартную xml-строку с описанием ошибки.
     */
    public static String getErrorMessage(String errorCode, String errorMessage, String... parameter) {
        String result = String.format("<data><error><code>%s</code><text>%s</text>", errorCode, errorMessage);
        if (parameter.length > 0) {
            result = result + "<parameter>" + parameter[0] + "</parameter>";
        }
        result = result + "</error></data>";

        return result;
    }

    /**
     * Возвращает новую пустую коллекцию с регистро-независимым поиском по ключам-строкам.
     *
     * @param <V> тип хранимых значений
     * @return новую пустую коллекцию с регистро-независимым поиском по ключам-строкам.
     */
    public static <V> LinkedCaseInsensitiveMap<V> newCaseInsensitiveMap() {
        return new LinkedCaseInsensitiveMap<>();
    }

}
