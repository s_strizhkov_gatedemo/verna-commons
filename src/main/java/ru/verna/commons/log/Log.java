package ru.verna.commons.log;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.verna.commons.Utils;

/**
 * Класс хранит ссылки на логгеры приложения.
 */
public final class Log {

    public static final String TEST_CALL_CONTEXT = "test";

    public static final String CALL_CONTEXT_HEADER = "Call-Context";

    public static final String USERNAME = "username";

    public static final String TX_NUMBER = "txNumber";

    public static final String CALL_CONTEXT = "callContext";

    public static final String UNKNOWN_USER = "unknown";
    /**
     * Основной логгер.
     */
    public static final Logger MAIN = LoggerFactory.getLogger("main");
    /**
     * Логгер взаимодействия с сервисом КИАС.
     */
    public static final Logger WS = LoggerFactory.getLogger("ws");
    /**
     * Логгер взаимодействия с БД.
     */
    public static final Logger DB = LoggerFactory.getLogger("db");
    /**
     * Логгер взаимодействия с внешним клиентом.
     */
    public static final Logger INOUT = LoggerFactory.getLogger("inout");
    /**
     * Логгер взаимодействия с внешними ресурсами.
     */
    public static final Logger WEB = LoggerFactory.getLogger("web");
    /**
     * Логгер для тестов.
     */
    public static final Logger TEST = LoggerFactory.getLogger("test");

    /**
     * Закрытый конструктор.
     */
    private Log() {
    }

    /**
     * Метод, возвращающий конкретный логгер. Если дело происходит во время запуска тестов
     * (переменная {@link #CALL_CONTEXT} установлена в {@link #TEST_CALL_CONTEXT}), метод вернет логгер {@code test}.
     *
     * @param name имя запрашиваемого логгера
     * @return экземпляр конкретного логгера
     */
    public static Logger getLogger(String name) {
        String threadCallContext = (String) ThreadUtil.getThreadVariable(CALL_CONTEXT);
        return TEST_CALL_CONTEXT.equalsIgnoreCase(threadCallContext) ? LoggerFactory.getLogger("test")
                : LoggerFactory.getLogger(name);
    }

    /**
     * Очищает json от значений указанных в log.properties.
     *
     * @param rawLog json в виде строки
     * @return очищенный json
     */
    public static String getFilteredLog(String rawLog) {
        DocumentContext documentContext = JsonPath.parse(rawLog);
        Utils.LOG_PROPERTIES.forEach((key, value) -> {
            if (!Boolean.parseBoolean((String) value)) {
                documentContext.delete("$." + key);
            }
        });

        return documentContext.jsonString();
    }
}
