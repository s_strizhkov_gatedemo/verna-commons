package ru.verna.commons.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * Конвертер для логера, используется для добавления в лог {@link ThreadLocal} переменной, имени пользователь пославшего
 * запрос.
 */
public class UsernameLogConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        return (String) ThreadUtil.getThreadVariable(Log.USERNAME);
    }
}
