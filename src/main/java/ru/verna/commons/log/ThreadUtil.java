package ru.verna.commons.log;

import java.util.HashMap;
import java.util.Map;

/**
 * Реализует работу с {@link ThreadLocal} переменными.
 */
public final class ThreadUtil {

    /**
     * Начальная инициализация, в качестве внутренней структуры используется экземпляр {@link HashMap}.
     */
    private static final ThreadLocal<Map<String, String>> THREAD_VARIABLES = ThreadLocal.withInitial(HashMap::new);

    /**
     * Закрытый конструктор.
     */
    private ThreadUtil() {
    }

    /**
     * Возвращает сохраненное во внутренней структуре значение переменной.
     *
     * @param name имя переменной
     * @return значение переменной или {@code null} если не найдено
     */
    public static Object getThreadVariable(String name) {
        return THREAD_VARIABLES.get().get(name);
    }

    /**
     * Добавляет или обновляет значение переменной во внутренней структуре.
     *
     * @param name имя переменной
     * @param value значение переменной
     */
    public static void setThreadVariable(String name, String value) {
        THREAD_VARIABLES.get().put(name, value);
    }

    /**
     * Возвращает все переменные в виде экземпляра {@link HashMap}.
     *
     * @return заполненный экземпляр {@link HashMap}
     */
    public static Map<String, String> getAllThreadVariables() {
        return THREAD_VARIABLES.get();
    }

    /**
     * Копирует все значения из переданной структуры во внутреннюю структуру текущего потока.
     *
     * @param map переданная для копирования структура
     */
    public static void putAllToThreadVariables(Map<String, String> map) {
        THREAD_VARIABLES.get().putAll(map);
    }

    /**
     * Очищает локальные переменные для текущего потока.
     */
    public static void destroy() {
        THREAD_VARIABLES.remove();
    }
}
