package ru.verna.commons;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import ru.verna.commons.log.Log;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;

/**
 * Методы общего назначения для работы с xml.
 */
public final class XmlUtils {

    private static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();
    public static XPathExpression dataErrorCodeXpath;
    public static XPathExpression dataResultSidXpath;
    public static XPathExpression dataResultXpath;
    public static XPathExpression dataRequestXpath;

    static {
        try {
            dataErrorCodeXpath = XPATH_FACTORY.newXPath().compile("/data/error/code");
            dataResultSidXpath = XPATH_FACTORY.newXPath().compile("/data/result/Sid");
            dataResultXpath = XPATH_FACTORY.newXPath().compile("/data/result");
            dataRequestXpath = XPATH_FACTORY.newXPath().compile("/data/request");
        } catch (XPathExpressionException e) {
            Log.getLogger("main").error(null, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Закрытый конструктор.
     */
    private XmlUtils() {
    }

    /**
     * Возвращает xml-ноду в виде строки.
     *
     * @param node xml-нода
     * @return строковое представление xml-ноды
     */
    public static String convertToString(Node node) {
        String result = null;
        TransformerFactory tf = TransformerFactory.newInstance();
        try (StringWriter writer = new StringWriter()) {
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(new DOMSource(node), new StreamResult(writer));
            String toString = writer.toString();
            result = toString.replaceAll(">[\\n]\\s*", ">");
        } catch (IOException | TransformerException e) {
            Log.getLogger("main").error(null, e);
        }
        return result;
    }

    /**
     * Находит и возвращает в родительском xml-узле дочерний узел с заданным именем. Если такой узел не найден, то
     * опционально (аргумент createIfNull) создает искомый дочерний узел.
     *
     * @param node родительский xml-узел
     * @param childName имя дочернего узла
     * @param createIfNull создавать дочерний узел, если он не найден
     * @return Node
     */
    public static Node getChildByName(Node node, String childName, boolean createIfNull) {
        if (node == null) {
            throw new IllegalArgumentException("node cannot be null");
        }
        if (childName == null || childName.length() == 0) {
            throw new IllegalArgumentException("childName cannot be empty");
        }
        Node child = node.getFirstChild();
        while (child != null && !childName.equalsIgnoreCase(child.getNodeName())) {
            child = child.getNextSibling();
        }
        if (createIfNull && child == null) {
            child = node.getOwnerDocument().createElement(childName);
            node.appendChild(child);
        }
        return child;
    }

    /**
     * Парсит строку в xml-документ.
     *
     * @param xmlAsString строка
     * @return строковое представление xml-документа.
     * @throws SAXException в случае проблем с парсингом xml
     * @throws IOException в случае проблем с перекодировкой строки
     */
    public static Document parseToXml(String xmlAsString) throws SAXException, IOException {
        Document result = null;
        try {
            result = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder()
                    .parse(new ByteArrayInputStream(xmlAsString.getBytes(Charset.forName("UTF-8"))));
        } catch (ParserConfigurationException e) {
            Log.getLogger("main").error(null, e);
        }
        return result;
    }

    /**
     * Возвращает значение определенной ноды.
     *
     * @param rawXmlString строковое представление xml-документа
     * @param nodeName имя ноды
     * @return значение ноды
     */
    public static String getNodeValue(String rawXmlString, String nodeName) {
        Node dataResultNode = null;
        Document dataAsXml;
        try {
            dataAsXml = parseToXml(rawXmlString);
            dataResultNode = (Node) dataResultXpath.evaluate(dataAsXml, XPathConstants.NODE);
        } catch (SAXException | IOException | XPathExpressionException e) {
            Log.getLogger("main").error(null, e);
        }
        Node fullNameNode = getChildByName(dataResultNode, nodeName, false);
        return fullNameNode.getTextContent();
    }
}
