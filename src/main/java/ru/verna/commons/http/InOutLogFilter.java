package ru.verna.commons.http;

import org.slf4j.Logger;
import ru.verna.commons.log.Log;
import ru.verna.commons.log.ThreadUtil;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Журналирует пришедший {@code request} и результирующий {@code response}.
 */
public class InOutLogFilter extends SkippableFilter {

    /**
     * Конструктор.
     */
    public InOutLogFilter() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
            throw new ServletException("Фильтр поддерживает только HTTP запросы");
        }
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        boolean skipRequest = skipRequest(httpServletRequest.getRequestURI());

        if (skipRequest) {
            chain.doFilter(request, response);
        } else {
            Logger inOutLogger = Log.getLogger("inout");
            Logger inOutDirtyLogger = Log.getLogger("inout-dirty");

            DescribingRequestWrapper describingRequestWrapper = new DescribingRequestWrapper(httpServletRequest);
            String requestDescr = describingRequestWrapper.getDescription();

            inOutLogger.info("REQUEST: {}", Log.getFilteredLog(requestDescr));
            if (!Log.TEST_CALL_CONTEXT.equalsIgnoreCase((String) ThreadUtil.getThreadVariable(Log.CALL_CONTEXT))) {
                inOutDirtyLogger.info("REQUEST: {}", requestDescr);
            }
            ResponseWrapper responseWrapper = new ResponseWrapper((HttpServletResponse) response);
            try {
                chain.doFilter(request, responseWrapper);
                String content = responseWrapper.getCaptureAsString();
                response.getWriter().write(content);
            } catch (IOException | ServletException e) {
                Log.getLogger("main").error(null, e);
            } finally {
                String responseDescr = responseWrapper.getDescription(false);
                inOutLogger.info("RESPONSE: {}", Log.getFilteredLog(responseDescr));
                if (!Log.TEST_CALL_CONTEXT.equalsIgnoreCase((String) ThreadUtil.getThreadVariable(Log.CALL_CONTEXT))) {
                    inOutDirtyLogger.info("RESPONSE: {}", responseDescr);
                }
            }
        }
    }
}
