package ru.verna.commons.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Обертка вокруг response'а с методом {@link #getDescription} для нужд логирования.
 */
public class ResponseWrapper extends HttpServletResponseWrapper {
    /**
     * JSON-конвертер.
     */
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ServletOutputStream output;
    private PrintWriter writer;
    private final ByteArrayOutputStream capture;

    /**
     * Конструктор.
     *
     * @param response отклик сервера
     */
    public ResponseWrapper(HttpServletResponse response) {
        super(response);
        capture = new ByteArrayOutputStream(response.getBufferSize());
    }

    @Override
    public ServletOutputStream getOutputStream() {
        if (writer != null) {
            throw new IllegalStateException("getWriter() has already been called on this response.");
        }

        if (output == null) {
            output = new ServletOutputStream() {
                @Override
                public void write(int b) throws IOException {
                    capture.write(b);
                }

                @Override
                public void flush() throws IOException {
                    capture.flush();
                }

                @Override
                public void close() throws IOException {
                    capture.close();
                }

                @Override
                public boolean isReady() {
                    return false;
                }

                @Override
                public void setWriteListener(WriteListener arg0) {
                }
            };
        }

        return output;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (output != null) {
            throw new IllegalStateException("getOutputStream() has already been called on this response.");
        }

        if (writer == null) {
            writer = new PrintWriter(new OutputStreamWriter(capture, getCharacterEncoding()));
        }

        return writer;
    }

    @Override
    public void flushBuffer() throws IOException {
        super.flushBuffer();

        if (writer != null) {
            writer.flush();
        } else if (output != null) {
            output.flush();
        }
    }

    /**
     * Возвращает содержимое отклика (его потока) в виде массива байтов.
     *
     * @return содержимое отклика в виде массива байтов.
     * @throws IOException в случае проблем с потоком
     */
    public byte[] getCaptureAsBytes() throws IOException {
        if (writer != null) {
            writer.close();
        } else if (output != null) {
            output.close();
        }

        return capture.toByteArray();
    }

    /**
     * Возвращает содержимое отклика (его потока) в виде строки.
     *
     * @return содержимое отклика в виде строки
     * @throws IOException в случае проблем с потоком
     */
    public String getCaptureAsString() throws IOException {
        return new String(getCaptureAsBytes(), getCharacterEncoding());
    }

    /**
     * Заголовки отклика.
     *
     * @return Заголовки отклика
     */
    private Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>(0);
        getHeaderNames().forEach((headerName) -> {
            headers.put(headerName, getHeader(headerName));
        });
        return headers;
    }

    /**
     * Возвращает описание отклика сервера.
     *
     *
     * @param showFullBody если true, то тело запроса будет показано в описании полностью, иначе тело будет обрезано до
     * 1024 первых символов.
     * @return описание отклика сервера.
     */
    public String getDescription(boolean showFullBody) {
        ResponseDescriptor responseDescriptor = new ResponseDescriptor();
        responseDescriptor.setStatus(getStatus());
        responseDescriptor.setHeaders(getHeaders());
        responseDescriptor.setContentType(getContentType());

        String content;
        try {
            content = getCaptureAsString();
        } catch (IOException e) {
            Log.getLogger("main").error(null, e);
            content = "[IOException]";
        }
        if (showFullBody) {
            responseDescriptor.setBody(content);
        } else {
            //CHECKSTYLE:OFF
            String body = Utils.restrictString(content, 1024);
            responseDescriptor.setBody(body);
            //CHECKSTYLE:ON
        }

        try {
            return OBJECT_MAPPER.writeValueAsString(responseDescriptor);
        } catch (JsonProcessingException e) {
            Log.getLogger("inout").warn("Cannot serialize Response to JSON", e);
            return null;
        }
    }
}
