package ru.verna.commons.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import ru.verna.commons.log.Log;
import ru.verna.commons.Utils;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Обертка вокруг request'а с методом {@link #getDescription} для нужд логирования.
 * <br>
 * После того, как запрос (request) оказался обернутым этим классом, запрещено обращаться к исходному запросу
 * (так как читать его можно только один раз, и это чтение делает класс-обертка).
 * https://github.com/librucha/servlet-logging-filter
 */
public class DescribingRequestWrapper extends HttpServletRequestWrapper {
    private static final String FORM_CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String METHOD_POST = "POST";
    private byte[] content;
    private final Map<String, String[]> parameterMap;
    private final HttpServletRequest delegate;

    /**
     * Конструктор.
     *
     * @param request оборачиваемый запрос
     */
    public DescribingRequestWrapper(HttpServletRequest request) {
        super(request);
        this.delegate = request;
        if (isFormPost()) {
            this.parameterMap = request.getParameterMap();
        } else {
            this.parameterMap = Collections.emptyMap();
        }
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (ArrayUtils.isEmpty(content)) {
            return delegate.getInputStream();
        }
        return new LoggingServletInputStream(content);
    }

    @Override
    public BufferedReader getReader() throws IOException {
        if (ArrayUtils.isEmpty(content)) {
            return delegate.getReader();
        }
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public String getParameter(String name) {
        if (ArrayUtils.isEmpty(content) || this.parameterMap.isEmpty()) {
            return super.getParameter(name);
        }
        String[] values = this.parameterMap.get(name);
        if (values != null && values.length > 0) {
            return values[0];
        }
        return Arrays.toString(values);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        if (ArrayUtils.isEmpty(content) || this.parameterMap.isEmpty()) {
            return super.getParameterMap();
        }
        return this.parameterMap;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        if (ArrayUtils.isEmpty(content) || this.parameterMap.isEmpty()) {
            return super.getParameterNames();
        }
        return new ParamNameEnumeration(this.parameterMap.keySet());
    }

    @Override
    public String[] getParameterValues(String name) {
        if (ArrayUtils.isEmpty(content) || this.parameterMap.isEmpty()) {
            return super.getParameterValues(name);
        }
        return this.parameterMap.get(name);
    }

    /**
     * Содержимое запроса.
     *
     * @return содержимое запроса в виде строки
     */
    private String getContent() {
        try {
            if (this.parameterMap.isEmpty()) {
                content = IOUtils.toByteArray(delegate.getInputStream());
            } else {
                content = getContentFromParameterMap(this.parameterMap);
            }
            String requestEncoding = delegate.getCharacterEncoding();
            String normalizedContent = StringUtils.normalizeSpace(
                    new String(content, requestEncoding != null ? requestEncoding : Charset.forName("UTF-8").name()));
            return StringUtils.isBlank(normalizedContent) ? "[EMPTY]" : normalizedContent;
        } catch (IOException e) {
            Log.getLogger("main").error(null, e);
            throw new IllegalStateException();
        }
    }

    /**
     * Получить содержимое коллекции параметров.
     *
     * @param parameterMap коллекция параметров
     * @return содержимое коллекции параметров.
     */
    private byte[] getContentFromParameterMap(Map<String, String[]> parameterMap) {
        return parameterMap.entrySet().stream().map(e -> {
            String[] value = e.getValue();
            return e.getKey() + "=" + (value.length == 1 ? value[0] : Arrays.toString(value));
        }).collect(Collectors.joining("&")).getBytes();
    }

    /**
     * Заголовки запроса.
     *
     * @return Заголовки запроса.
     */
    private Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>(0);
        Enumeration<String> headerNames = getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if (headerName != null) {
                headers.put(headerName, getHeader(headerName));
            }
        }
        return headers;
    }

    /**
     * Параметры запроса.
     *
     * @return Параметры запроса
     */
    private Map<String, String> getParameters() {
        return getParameterMap().entrySet().stream().collect(Collectors.toMap(Entry::getKey, e -> {
            String[] values = e.getValue();
            return values.length > 0 ? (values.length == 1 ? values[0] : Arrays.toString(values)) : "[EMPTY]";
        }));
    }

    /**
     * Возвращает true, если запрос представляет собой форму, отправленную методом POST.
     *
     * @return true, если запрос представляет собой форму, отправленную методом POST, иначе false.
     */
    private boolean isFormPost() {
        String contentType = getContentType();
        return (contentType != null && contentType.contains(FORM_CONTENT_TYPE)
                && METHOD_POST.equalsIgnoreCase(getMethod()));
    }

    /**
     * Возвращает описание запроса в json-формате.
     *
     * @param bodyRestriction добавлять ли в описание полное тело запроса (0 или отсутствие аргумента) или ограничить
     *            тело запроса (положительный - выдать
     *            начало тела, отрицательный - выдать конец тела).
     * @return описание запроса в json-формате
     */
    public String getDescription(int... bodyRestriction) {
        RequestDescriptor requestDescriptor = new RequestDescriptor();
        requestDescriptor.setSender(this.getLocalAddr());
        requestDescriptor.setMethod(this.getMethod());
        requestDescriptor.setPath(this.getRequestURI());
        requestDescriptor.setParams(this.isFormPost() ? null : this.getParameters());
        requestDescriptor.setHeaders(this.getHeaders());

        String _content = this.getContent();
        _content = Utils.restrictString(_content, bodyRestriction);
        requestDescriptor.setBody(_content);

        try {
            return requestDescriptor.describe();
        } catch (JsonProcessingException e) {
            return e.getLocalizedMessage();
        }
    }

    /**
     * Имплементация Enumeration&lt;String&gt; для нужд основного класса.
     */
    private final class ParamNameEnumeration implements Enumeration<String> {
        private final Iterator<String> iterator;

        /**
         * Конструктор.
         *
         * @param values коллекция строк
         */
        private ParamNameEnumeration(Set<String> values) {
            this.iterator = values != null ? values.iterator() : Collections.emptyIterator();
        }

        @Override
        public boolean hasMoreElements() {
            return iterator.hasNext();
        }

        @Override
        public String nextElement() {
            return iterator.next();
        }
    }

    /**
     * Поток для нужд основного класса.
     */
    private final class LoggingServletInputStream extends ServletInputStream {
        private final InputStream is;

        /**
         * Конструктор.
         *
         * @param content массив байтов, которые надо читать
         */
        private LoggingServletInputStream(byte[] content) {
            this.is = new ByteArrayInputStream(content);
        }

        @Override
        public boolean isFinished() {
            return true;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {
        }

        @Override
        public int read() throws IOException {
            return this.is.read();
        }

        @Override
        public void close() throws IOException {
            super.close();
            is.close();
        }
    }

}
