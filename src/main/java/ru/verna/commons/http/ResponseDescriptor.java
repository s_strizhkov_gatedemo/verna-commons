package ru.verna.commons.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import java.io.Serializable;
import java.util.Map;

/**
 * Модель данных о response'е для логирования.
 */
public class ResponseDescriptor implements Serializable {
    private static final long serialVersionUID = -6692682176015358216L;
    /**
     * JSON-конвертер.
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    // CHECKSTYLE:OFF
    private int status;
    private String contentType;
    private Map<String, String> headers;
    private String body;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    /**
     * Двойные кавычки меняются на одинарные для чистоты вывода.
     *
     * @param body
     */
    public void setBody(String body) {
        this.body = body;
        this.body = this.body.replace('"', '\'');
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    // CHECKSTYLE:ON

    /**
     * Возвращает json-описание самого себя.
     * <br>
     * JSON-сериализатор бежит по get-методам, поэтому этот метод не должен быть геттером (во избежании бесконечного
     * цикла извлечения собственного значения).
     *
     * @return json-описание самого себя
     * @throws JsonProcessingException в случае проблем
     */
    public String describe() throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(this);
    }

    /**
     * Возвращает описание отклика сервера.
     *
     * @param showFullBody если true, то тело запроса будет показано в описании полностью, иначе тело будет обрезано до
     *            1024 первых символов.
     * @return описание отклика сервера.
     */
    public String describe(boolean showFullBody) {
        if (!showFullBody) {
            String body = Utils.restrictString(getBody(), 1024);
            this.setBody(body);
        }

        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            Log.getLogger("inout").warn("Cannot serialize Response to JSON", e);
            return null;
        }
    }
}
