package ru.verna.commons.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.net.ssl.SSLContext;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;

/**
 * Класс создающий и настраивающий общий HTTP клиент для работы приложения.
 */
public final class CommonHttpClient {

    /**
     * Default constructor.
     */
    private CommonHttpClient() {
    }

    /**
     * Отправляет GET- или POST-запрос по адресу url с параметрами parameters, возвращает отклик сервера в виде строки.
     * <br>
     * К GET-запросам параметры из parameters дописываются к url после символа "?",
     * поэтому либо строка url должна быть без "?", то есть не содержать в себе
     * какие-либо параметры, либо коллекция parameters должна быть пустая или null.
     *
     * @param httpMethod http-метод запроса
     * @param url        адрес запрашиваемого ресурса
     * @param parameters параметры запроса
     * @param headers    заголовки запроса
     * @return http-отклик на отправленный запрос
     * @throws Exception в случае проблем
     */
    public static String send(String httpMethod, String url, Map<String, String> parameters, Map<String, String> headers)
            throws Exception {
        Logger webLogger = Log.getLogger("web");

        if (Utils.isEmpty(httpMethod)
                ||
                !(httpMethod.equalsIgnoreCase(HttpGet.METHOD_NAME) || httpMethod.equalsIgnoreCase(HttpPost.METHOD_NAME))) {
            // не GET и не POST = проблемный httpMethod
            webLogger.warn("Задан некорректный http-метод: {}", httpMethod);
            throw new IllegalArgumentException("Задан некорректный http-метод: " + httpMethod);
        }

        CloseableHttpClient httpClient = (CloseableHttpClient) getHttpClient();
        HttpRequestBase httpRequest;

        RequestDescriptor requestDescriptor = new RequestDescriptor();
        requestDescriptor.setSender("-");

        String result = "";

        if (httpMethod.equalsIgnoreCase(HttpGet.METHOD_NAME)) {
            // для GET-запроса параметры передаем в query-string
            String paramsString = Utils.joinParameters(parameters);
            String resultedUrl = url + (paramsString.length() > 0 ? "?" + paramsString : "");
            httpRequest = new HttpGet(resultedUrl);

        } else if (httpMethod.equalsIgnoreCase(HttpPost.METHOD_NAME)) {
            // для POST-запроса параметры передаем в теле запроса
            httpRequest = new HttpPost(url);

            if (parameters != null && !parameters.isEmpty()) {
                List<NameValuePair> params = new ArrayList<>();
                parameters.forEach((key, val) -> params.add(new BasicNameValuePair(key, val)));
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, StandardCharsets.UTF_8);
                httpRequest.removeHeaders("Content-Length");
                ((HttpPost) httpRequest).setEntity(entity);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                entity.writeTo(baos);
                String entityBody = new String(baos.toByteArray(), StandardCharsets.UTF_8);
                requestDescriptor.setBody(entityBody);
            }

        } else {
            // недостижимая строка кода
            throw new IllegalArgumentException("Задан некорректный http-метод: " + httpMethod);
        }

        if (headers != null) {
            headers.entrySet()
                    .stream()
                    .filter((entry) -> !"Content-Length".equalsIgnoreCase(entry.getKey()))
                    .forEach(entry -> httpRequest.addHeader(entry.getKey(), entry.getValue()));
        }
        Map<String, String> headersMap =
                Arrays.stream(httpRequest.getAllHeaders())
                        .collect(Collectors.toMap(e -> e.getName(), e -> e.getValue()));
        requestDescriptor.setHeaders(headersMap);
        webLogger.debug("Входные заголовки: {}", headers);
        webLogger.debug("Отправляемые заголовки: {}", headersMap);

        requestDescriptor.setMethod(httpRequest.getMethod().toUpperCase());

        webLogger.debug("Входной url: {}", url);
        webLogger.debug("Отправляемый url: {}", httpRequest.getURI().toString());
        requestDescriptor.setPath(httpRequest.getURI().toASCIIString());

        Map<String, String> paramsWithDecodedPswd = copyMapWithDecodedPassword(parameters);
        requestDescriptor.setParams(paramsWithDecodedPswd);
        webLogger.debug("Входные параметры: {}", paramsWithDecodedPswd);


        String requestDescr;
        try {
            requestDescr = requestDescriptor.describe();
        } catch (JsonProcessingException e) {
            requestDescr = e.getLocalizedMessage();
        }
        // типа уникальный id транзакции запрос-ответ
        long txNumber = Utils.getRandomLong();
        webLogger.info("tx {} REQUEST: {}", txNumber, requestDescr);
        try (CloseableHttpResponse response = httpClient.execute(httpRequest)) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                int statusCode = response.getStatusLine().getStatusCode();
                String contentType = entity.getContentType().getValue();
                Map<String, String> responseHeaders = new TreeMap<>();
                for (Header header : response.getAllHeaders()) {
                    responseHeaders.put(header.getName(), header.getValue());
                }
                ResponseDescriptor responseDescriptor = new ResponseDescriptor();
                responseDescriptor.setStatus(statusCode);
                responseDescriptor.setHeaders(responseHeaders);
                responseDescriptor.setContentType(contentType);
                responseDescriptor.setBody(result);
                String responseDescr = responseDescriptor.describe(false);
                webLogger.info("tx {} RESPONSE: {}", txNumber, responseDescr);
            }
        } catch (Exception e) {
            webLogger.error("tx {" + txNumber + "} RESPONSE ERROR:", e);
            throw e;
        }
        return result;
    }

    /**
     * Если в параметрах присутствует пароль, то оставляем от пароля только первые четыре символа, остальное заменяем звездочками.
     *
     * @param srcMap переданные параметры
     * @return копию переданных параметров с закодированым паролем, если он присутсвовал в параметрах
     */
    static Map<String, String> copyMapWithDecodedPassword(Map<String, String> srcMap) {
        if (srcMap == null) {
            return null;
        }
        Map<String, String> result = new HashMap<>(srcMap);
        String password = "password";
        if (result.containsKey(password)) {
            String passValue = result.get(password);
            if (Utils.notEmpty(passValue)) {
                result.put(password,
                        (passValue.length() > 1 ? passValue.substring(0, passValue.length() / 2) + "****" : "****"));
            }
        }
        return result;
    }

    /**
     * Создаем и настраиваем HTTP клиент для работы по HTTPS.
     *
     * @return экземпляр {@link HttpClient}
     */
    public static HttpClient getHttpClient() {
        try {
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustStrategy() {

                @Override
                public boolean isTrusted(final X509Certificate[] chain, final String authType) {
                    return true;
                }
            }).build();
            HttpClientBuilder builder = HttpClientBuilder.create();
            SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(
                    sslContext.getSocketFactory(), new NoopHostnameVerifier());
            builder.setSSLSocketFactory(sslConnectionFactory);
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("https", sslConnectionFactory)
                    .register("http", new PlainConnectionSocketFactory())
                    .build();
            HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
            builder.setConnectionManager(ccm);
            return builder.build();

        } catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException e) {
            Log.getLogger("main").error(null, e);
            return null;
        }
    }
}
