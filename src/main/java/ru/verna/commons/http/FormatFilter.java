package ru.verna.commons.http;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.XML;
import org.slf4j.Logger;
import ru.verna.commons.log.Log;

/**
 * Устанавливает нужный формат исходящих сообщений.
 */
public class FormatFilter extends SkippableFilter {

    /**
     * Конструктор.
     */
    public FormatFilter() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        Logger mainLogger = Log.getLogger("main");

        if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
            throw new ServletException("Фильтр поддерживает только HTTP запросы");
        }
        boolean skipRequest = skipRequest(httpServletRequest.getRequestURI());

        if (skipRequest) {
            chain.doFilter(request, response);

        } else {
            // разрешенные имена параметра смены формата на xml
            final String[] formatParams = {"f", "format"};
            // разрешенные значения параметра смены формата на xml
            final String[] formatValues = {"xml", "x"};
            boolean outputToJSON = true;
            find: for (String formatParam : formatParams) {
                for (String formatValue : formatValues) {
                    if (formatValue.equalsIgnoreCase(request.getParameter(formatParam))
                            || formatValue.equalsIgnoreCase(httpServletRequest.getHeader(formatParam))) {
                        outputToJSON = false;
                        break find;
                    }
                }
            }

            if (outputToJSON) {
                try {
                    ResponseWrapper capturingResponseWrapper = new ResponseWrapper((HttpServletResponse) response);
                    chain.doFilter(request, capturingResponseWrapper);

                    String xmlContent = capturingResponseWrapper.getCaptureAsString();
                    String convertedToJSON = XML.toJSONObject(xmlContent, true).toString();

                    response.setContentType("application/json;charset=UTF-8");
                    response.getWriter().write(convertedToJSON);
                } catch (IOException | ServletException | JSONException e) {
                    mainLogger.error(null, e);
                }

            } else {
                // ничего не преобразовываем, возвращаем xml
                response.setContentType("text/xml;charset=UTF-8");
                try {
                    chain.doFilter(request, response);
                } catch (IOException | ServletException e) {
                    mainLogger.error(null, e);
                }
            }
        }
    }
}
