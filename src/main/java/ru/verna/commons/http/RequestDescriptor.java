package ru.verna.commons.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.Map;

/**
 * Модель данных о request'е для логирования.
 */
public class RequestDescriptor implements Serializable {
    private static final long serialVersionUID = -4702574169916528738L;
    /**
     * JSON-конвертер.
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private String sender;
    private String method;
    private String path;
    private Map<String, String> params;
    private Map<String, String> headers;
    private String body;

    //CHECKSTYLE:OFF
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
        this.body = this.body.replace('"', '\'');
    }
    //CHECKSTYLE:ON

    /**
     * Возвращает json-описание самого себя.
     * <br>
     * JSON-сериализатор бежит по get-методам, поэтому сам этот метод не должен быть геттером (во избежании бесконечного
     * цикла извлечения собственного значения).
     *
     * @return json-описание самого себя
     * @throws JsonProcessingException в случае проблемы с обработкой json
     */
    public String describe() throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(this);
    }

}
