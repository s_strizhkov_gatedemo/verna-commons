package ru.verna.commons.http;

import ru.verna.commons.log.Log;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Фильтр с проверкой на пропуск. В web.xml задаются {@code <init-param>} с условиями пропуска фильтра.
 */
public abstract class SkippableFilter implements Filter {
    private FilterConfig filterConfig = null;

    /**
     * Возвращает true, если запрос к ресурсу по адресу requestURI должен быть пропущен фильтром без обработки.
     *
     * @param requestURI адрес запрошенного ресурса
     * @return true, если запрос к ресурсу по адресу requestURI должен быть пропущен фильтром без обработки, и false,
     *         если запрос должен быть обработан фильтром
     * @throws ServletException в случае проблем с http
     * @throws IOException в случае проблем с потоками
     */
    protected boolean skipRequest(String requestURI) throws ServletException, IOException {
        boolean skipPatternFound = false;
        if (getFilterConfig() == null) {
            return skipPatternFound;
        }
        Enumeration<String> initParameterNames = getFilterConfig().getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String next = initParameterNames.nextElement();
            if (next.startsWith("skipPattern")) {
                String paramValue = filterConfig.getInitParameter(next);
                if (requestURI.contains(paramValue)) {
                    skipPatternFound = true;
                    Log.getLogger("main").trace("filter {} skip [{}] by pattern [{}]", this.toString(),
                            requestURI, paramValue);
                    break; // break while loop
                }
            }
        }
        return skipPatternFound;
    }

    /**
     * Destroy method for this filter.
     */
    @Override
    public void destroy() {
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        String name = this.getClass().getName();
        name = name.substring(name.lastIndexOf(".") + 1);
        StringBuffer sb = new StringBuffer(name).append("(");
        if (filterConfig != null) {
            sb.append(filterConfig);
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * Init method for this filter.
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Return the filter configuration object for this filter.
     *
     * @return the filter configuration object
     */
    protected FilterConfig getFilterConfig() {
        return this.filterConfig;
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    protected void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }
}
