package ru.verna.commons.http;

import ru.verna.commons.log.Log;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Устанавливает нужную кодировку запросу и отклику.
 */
public class CharacterEncodingFilter extends SkippableFilter {

    /**
     * Конструктор.
     */
    public CharacterEncodingFilter() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
            throw new ServletException("Фильтр поддерживает только HTTP запросы");
        }
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        boolean skipRequest = skipRequest(httpServletRequest.getRequestURI());

        if (skipRequest) {
            chain.doFilter(request, response);
        } else {
            try {
                request.setCharacterEncoding("UTF-8");
                response.setCharacterEncoding("UTF-8");
                chain.doFilter(request, response);
            } catch (IOException | ServletException e) {
                Log.getLogger("main").error(null, e);
            }
        }
    }
}
