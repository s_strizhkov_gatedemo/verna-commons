package ru.verna.commons;

import java.time.LocalDate;

/**
 * Утилитный класс по работе с датами.
 */
public final class DateUtils {

    /**
     * Приватный дефолтный конструктор.
     */
    private DateUtils() {
    }

    /**
     * Форматирует переданную дату в формат 'dd.MM.yyyy' и возвращает строковое представление.
     *
     * @param localDate экземпляр {@link LocalDate}
     * @return отформатированное строковое представление переданной даты
     */
    public static String formatLocalDate(LocalDate localDate) {
        return localDate.format(Utils.DATE_FORMATTER);
    }
}
