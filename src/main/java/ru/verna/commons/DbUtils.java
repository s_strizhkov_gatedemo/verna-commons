package ru.verna.commons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import ru.verna.commons.log.Log;

/**
 * Методы общего назначения для работы с БД.
 */
public final class DbUtils {

    private static final String DATASOURCE_JNDI_NAME = Utils.APP_PROPERTIES.getProperty("datasource.jndi.name");

    /**
     * Default constructor.
     */
    private DbUtils() {
    }

    /**
     * Возвращает готовый к употреблению connection.
     *
     * @return Connection коннект к БД; null - в плохих случаях.
     */
    public static synchronized Connection getConnection() {
        // получение соединения из пула
        Connection connection = null;
        Logger dbLogger = Log.getLogger("db");
        try {
            Context initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup(DATASOURCE_JNDI_NAME);
            connection = ds.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException | NamingException e) {
            dbLogger.warn(e.getLocalizedMessage());
            throw new RuntimeException(e.toString());
        }
        dbLogger.trace("вернул соединение {}", connection);
        return connection;
    }

    /**
     * Возвращает хранимые настройки (таблица {@code config}) заданной категории. Имена параметров регистро-независимы.
     *
     * @param category категория настроек
     * @return хранимые в БД настройки заданной категории. Имена параметров регистро-независимы.
     */
    public static Map<String, String> getConfigs(String category) {
        Logger dbLogger = Log.getLogger("db");
        Map<String, String> result = Utils.newCaseInsensitiveMap();
        String sql = "SELECT name, value FROM env.config WHERE lower(category) = lower(?)";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, category);
            dbLogger.debug("|{}|, args: category={}", sql, category);

            ResultSet rs = ps.executeQuery();
            int cnt = 0;
            while (rs.next()) {
                cnt++;
                String name = rs.getString(1);
                String value = rs.getString(2);
                dbLogger.trace("returned row: name={}, value={}", name, value);
                result.put(name, value);
            }
            dbLogger.debug("return {} row(s)", cnt);
        } catch (SQLException e) {
            dbLogger.error(null, e);
        }
        return result;
    }
}
