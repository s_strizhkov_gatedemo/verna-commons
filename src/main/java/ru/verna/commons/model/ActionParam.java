package ru.verna.commons.model;

import ru.verna.commons.Utils;

/**
 * Параметр (аргумент) вызываемого метода сервиса КИАС.
 */
public class ActionParam {
    /**
     * Внешнее имя параметра. С таким именем он прилетает от клиента.
     */
    private String publicName;

    /**
     * Внутреннее имя параметра. С таким именем он улетает в КИАС.
     */
    private String kiasName;

    /**
     * Описание параметра.
     */
    private String descr;

    /**
     * Тип параметра (из справочника paramtypes).
     */
    private String typeId;

    /**
     * Обязательность параметра.
     */
    private boolean required;

    /**
     * Значение параметра.
     */
    private Object value;

    /**
     * Минимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     */
    private Integer minlength;

    /**
     * Максимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     */
    private Integer maxlength;

    /**
     * Маска, которой должно соответствовать значение параметра.
     */
    private String mask;

    /**
     * Хэш передаваемый с фронтенда используемый для идентификации заполняемого поля.
     */
    private String hash;
    /**
     * Параметр существует в таблице action_params.
     */
    private boolean confirmed = false;

    /**
     * Сокращенный конструктор.
     *
     * @param publicName Внешнее имя параметра. С таким именем он прилетает от клиента.
     * @param kiasName Внутреннее имя параметра. С таким именем он улетает в КИАС.
     * @param value Значение параметра.
     */
    public ActionParam(String publicName, String kiasName, String value) {
        setPublicName(publicName);
        setKiasName(kiasName);
        setValue(value);
    }

    /**
     * Создание нового параметра - копии переданного параметра copied.
     *
     * @param copied копируемый объект
     */
    public ActionParam(ActionParam copied) {
        this.copy(copied);
    }

    /**
     * Полный конструктор.
     *
     * @param publicName Внешнее имя параметра. С таким именем он прилетает от клиента.
     * @param kiasName Внутреннее имя параметра. С таким именем он улетает в КИАС.
     * @param descr Описание параметра.
     * @param typeId Тип параметра (из справочника paramtypes).
     * @param required Обязательность параметра.
     * @param minlength Минимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     * @param maxlength Максимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     * @param mask Маска, которой должно соответствовать значение параметра.
     */
    public ActionParam(String publicName, String kiasName, String descr, String typeId, boolean required,
            Integer minlength, Integer maxlength, String mask) {
        setPublicName(publicName);
        setKiasName(kiasName);
        setDescr(descr);
        setTypeId(typeId);
        setRequired(required);
        setMinlength(minlength);
        setMaxlength(maxlength);
        setMask(mask);
    }

    @Override
    public String toString() {
        return String.format("ActionParam[pubName=%s, kiasName=%s, type=%s, req=%s, cnfrm=%s, val=%s]", this.publicName,
                this.kiasName, this.typeId, this.required, this.confirmed, this.value);
    }

    /**
     * @return the name
     */
    public String getPublicName() {
        return publicName;
    }

    /**
     * @param publicName the name to set
     */
    public void setPublicName(String publicName) {
        if (Utils.isEmpty(publicName)) {
            throw new IllegalStateException("publicName must be not empty");
        }
        this.publicName = publicName;
    }

    /**
     * @return the kiasName
     */
    public String getKiasName() {
        return kiasName;
    }

    /**
     * @param kiasName the kiasName to set
     */
    public void setKiasName(String kiasName) {
        this.kiasName = kiasName;
    }

    /**
     * @return the descr
     */
    public String getDescr() {
        return descr;
    }

    /**
     * @param descr the descr to set
     */
    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        if (value == null || this.typeId == null) {
            this.value = value;

        } else {
            switch (this.typeId) {
                case "long":
                    try {
                        this.value = Long.parseLong(String.valueOf(value));
                    } catch (NumberFormatException nfe) {
                        this.value = value;
                    }
                    break;
                case "numeric":
                    try {
                        this.value = Double.parseDouble(String.valueOf(value));
                    } catch (NumberFormatException nfe) {
                        this.value = value;
                    }
                    break;
                case "date":
                    // храним даты как строки, т.к. JSONObject не обрабатывает даты
                    this.value = value;
                    break;
                default:
                    this.value = value;
            }
        }
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * Минимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     *
     * @return Минимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     */
    public Integer getMinlength() {
        return minlength;
    }

    /**
     * @param minlength the maxlength to set
     */
    public void setMinlength(Integer minlength) {
        this.minlength = minlength;
    }

    /**
     * Максимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     *
     * @return Максимальная длина текстового представления параметра. Если ноль, то длина не ограничена.
     */
    public Integer getMaxlength() {
        return maxlength;
    }

    /**
     * @param maxlength the maxlength to set
     */
    public void setMaxlength(Integer maxlength) {
        this.maxlength = maxlength;
    }

    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * Возвращает строковое представление значения параметра. Если значение равно null, метод вернет пустую строку.
     *
     * @return строковое представление значения параметра. Если значение равно null, метод вернет пустую строку.
     */
    public String getValueAsString() {
        Object objValue = this.getValue();
        String valueAsString = objValue != null ? objValue.toString() : "";
        return valueAsString;
    }

    /**
     * Принимает все атрибуты переданного объекта.
     *
     * @param anotherActionParam копируемый объект
     */
    public void copy(ActionParam anotherActionParam) {
        this.setDescr(anotherActionParam.getDescr());
        this.setKiasName(anotherActionParam.getKiasName());
        this.setMask(anotherActionParam.getMask());
        this.setMinlength(anotherActionParam.getMinlength());
        this.setMaxlength(anotherActionParam.getMaxlength());
        this.setPublicName(anotherActionParam.getPublicName());
        this.setRequired(anotherActionParam.isRequired());
        this.setTypeId(anotherActionParam.getTypeId());
        this.setValue(anotherActionParam.getValue());
        this.setConfirmed(anotherActionParam.isConfirmed());
    }

    /**
     * Параметр существует в таблице action_params.
     *
     * @return the confirmed
     */
    public boolean isConfirmed() {
        return confirmed;
    }

    /**
     * Параметр существует в таблице action_params.
     *
     * @param confirmed the confirmed to set
     */
    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * Хэш передаваемый с фронтенда используемый для идентификации заполняемого поля.
     *
     * @return хэш поля
     */
    public String getHash() {
        return hash;
    }

    /**
     * Устанавливает хэш для идентификации заполняемого поля.
     *
     * @param hash значение хэша
     */
    public void setHash(String hash) {
        this.hash = hash;
    }
}
