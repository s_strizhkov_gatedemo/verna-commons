package ru.verna.commons.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.paramchecker.ErrorList;
import ru.verna.commons.model.paramchecker.ParamChecker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 */
public class ParamStructure {
    private static final String SPLITTER = "-";

    /**
     * Коллекция параметров в виде глубокой структуры. Ключи - строки (имена параметров), значения могут быть:
     * <br>
     * <ul>
     * <li>коллекцией Map&lt;String, Object&gt;</li>
     * <li>массивом коллекций Map&lt;String, Object&gt;</li>
     * <li>ActionParam'ом</li>
     * <li>строкой</li>
     * </ul>
     */
    private Map<String, Object> structure = Utils.newCaseInsensitiveMap();

    /**
     * Содержит ли коллекция массивы.
     */
    private boolean containsArrays;

    /**
     * Является ли переданная коллекция параметров плоской, то есть не содержит ли вложенные коллекции.
     *
     * @param struct обрабатываемая структура
     * @return true, если коллекция является плоской, то есть не содержит вложенные коллекции
     */
    public static boolean isFlatStructure(Map<String, Object> struct) {
        for (Object val : struct.values()) {
            if ((val instanceof Map) || (val instanceof List)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [" + this.structure.toString() + "]";
    }

    /**
     * Возвращает хранимую структуру. Структуру можно только смотреть, менять нельзя.
     *
     * @return хранимую структуру параметров для тестовых проверок.
     */
    public Map<String, Object> getStructure() {
        return Collections.unmodifiableMap(this.structure);
    }

    /**
     * Возвращает всю структуру в виде json-строки.
     *
     * @return структуру в виде json-строки.
     * @throws JSONException в случае json-ошибок
     */
    String describeAsJson() throws JSONException {
        JSONObject jsonObj = new JSONObject(structure);
        String result = jsonObj.toString(4);
        return result;
    }

    /**
     * Возвращает всю структуру в виде xml-фрагмента.
     *
     * @return структуру в виде json-строки.
     * @throws JSONException в случае json-ошибок
     */
    String describeAsXml() throws JSONException {
        JSONObject jsonObj = new JSONObject(structure);
        String convertedToXML = XML.toString(jsonObj);
        return convertedToXML;
    }

    /**
     * Читает параметры из переданной коллекции параметров запрос. Заполняет плоскую или глубокую коллекцию строками.
     *
     * @param sourceMap map, из которого берется наполнение выдаваемой структуры.
     * @throws JSONException в случае json-ошибок
     */
    public void readRequest(Map<String, String[]> sourceMap) throws JSONException {
        if (sourceMap != null) {
            if (sourceMap.containsKey("params")) {
                // режим json: параметры пришли в json-структуре под именем params
                String params = sourceMap.get("params")[0];
                try {
                    readJSON(params);
                } catch (JSONException e) {
                    Log.getLogger("main").warn("Неверная json-структура в параметре params |{}|", params);
                    throw e;
                }

            } else {
                // стандартный режим: параметры передаются линейным списком
                sourceMap.keySet().forEach(
                        (String key) -> {
                            String[] val = sourceMap.get(key);
                            String value = val.length > 0 ? val[0] : null;
                            this.add(key, value);
                        });
            }
        }
    }

    /**
     * Перегруженный метод {@link ParamStructure#readRequest(Map)} Читает параметры из переданной коллекции параметров
     * запрос. Заполняет плоскую или глубокую коллекцию строками.
     *
     * @param sourceMap map, из которого берется наполнение выдаваемой структуры.
     * @param skipParams если true то обрабатывает в стандартном режиме, несмотря на наличие параметра params
     * @throws JSONException в случае json-ошибок
     */
    public void readRequest(Map<String, String[]> sourceMap, boolean skipParams) throws JSONException {
        if (sourceMap != null) {
            if (sourceMap.containsKey("params") && !skipParams) {
                // режим json: параметры пришли в json-структуре под именем params
                String params = sourceMap.get("params")[0];
                try {
                    readJSON(params);
                } catch (JSONException e) {
                    Log.getLogger("main").error("Неверная json-структура в параметре params |" + params + "|", e);
                    throw e;
                }

            } else {
                // стандартный режим: параметры передаются линейным списком
                sourceMap.keySet().forEach(
                        (String key) -> {
                            String[] val = sourceMap.get(key);
                            String value = val.length > 0 ? val[0] : null;
                            this.add(key, value);
                        });
            }
        }
    }

    /**
     * Создает структуру по заданному XML.
     *
     * @param xml строковое представление XML
     * @throws JSONException в случае json-ошибок
     */
    public void readXML(String xml) throws JSONException {
        JSONObject jsonObj = XML.toJSONObject(xml);
        String json = jsonObj.toString();
        readJSON(json);
    }

    /**
     * Создает структуру по заданному json'у.
     *
     * @param json json-строка
     * @throws JSONException в случае json-ошибок
     */
    @SuppressWarnings("unchecked")
    public void readJSON(String json) throws JSONException {
        JSONObject jsonObj = new JSONObject(json);
        Object parsed = parse(jsonObj);
        this.structure = (Map<String, Object>) parsed;
    }

    /**
     * Разбирает json-структуру параметров в объект. Тип результирующего объекта:
     * <ul>
     * <li>если входящий аргумент JSONObject, то Map&lt;String, Object&gt; с листьями типа ActionParam</li>
     * <li>если входящий аргумент JSONArray, то List&lt;Object&gt; с элементами типа ActionParam или String</li>
     * <li>в остальных случаях ActionParam</li>
     * </ul>
     *
     * @param parsedObject json-структура с параметрами (именами и значениями)
     * @param parentPrefix рекурсивно заполняемое имя родительского элемента
     * @return Если входящий аргумент JSONObject, то Map&lt;String, Object&gt; с листьями типа ActionParam. Если
     *         входящий аргумент JSONArray, то List&lt;Object&gt; с элементами типа ActionParam или String. В остальных
     *         случаях
     *         ActionParam.
     */
    private Object parse(Object parsedObject, String... parentPrefix) {
        String _parentPrefix = parentPrefix.length > 0 ? parentPrefix[0] : "";
        Logger mainLogger = Log.getLogger("main");
        mainLogger.trace("called with prefix {}", _parentPrefix);
        if (parsedObject instanceof JSONObject) {
            // собираем и возвращаем Map<String, Object>
            Map<String, Object> result = Utils.newCaseInsensitiveMap();
            mainLogger.trace("create map #{}", result.hashCode());
            JSONObject parsedJSONObject = (JSONObject) parsedObject;
            Iterator<String> keys = parsedJSONObject.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                Object value = null;
                try {
                    value = parsedJSONObject.get(key);
                } catch (JSONException e) {
                    // key неожиданно not found
                }
                mainLogger.trace("filling map #{}: found key={}, value={}", result.hashCode(), key, value);
                String fullKey = (_parentPrefix.length() > 0 ? _parentPrefix + SPLITTER : "") + key;
                Object innerObj = parse(value, fullKey);
                if (value instanceof JSONObject) {
                    // Map<String, Object> innerMap = (Map<String, Object>) innerObj;
                    mainLogger.trace("filling map #{}: put key={}, inner map={}", result.hashCode(), key, innerObj);

                } else if (value instanceof JSONArray) {
                    this.setContainsArrays(true);
                    // List<Object> innerArray = (List<Object>) innerObj;
                    mainLogger.trace("filling map #{}: put key={}, inner array={}", result.hashCode(), key, innerObj);

                } else {
                    String stringObj = String.valueOf(innerObj);
                    innerObj = stringObj;
                    mainLogger.trace("filling map #{}: put leaf key={}, string={}", result.hashCode(), key, innerObj);
                }
                result.put(key, innerObj);
            }
            mainLogger.trace("resulted map #{}, value is {}", result.hashCode(), result.toString());
            return result;

        } else if (parsedObject instanceof JSONArray) {
            JSONArray parsedJSONArray = (JSONArray) parsedObject;
            int len = parsedJSONArray.length();
            List<Object> result = new ArrayList<>(len);
            mainLogger.trace("create array #{}, length is {}", result.hashCode(), len);
            for (int i = 0; i < len; i++) {
                Object element = null;
                try {
                    element = parsedJSONArray.get(i);
                } catch (JSONException ex) {
                    // ith element неожиданно not found
                }
                mainLogger.trace("filling array #{}: found element={}, value={}", result.hashCode(), i, element);
                Object obj = parse(element, _parentPrefix);
                result.add(i, obj);
            }
            mainLogger.trace("resulted array #{}, value is {}", result.hashCode(), result.toString());
            return result;

        } else {
            String result = String.valueOf(parsedObject);
            mainLogger.trace("resulted string={}", result);
            return result;
        }
    }

    /**
     * Возвращает json-строку, где листья - значения параметров.
     * <br>
     * <ul>
     * <li>
     * Если структура параметров плоская (является изначально или стала плоской по аргументу {@code flatten}), имена
     * параметров будут взяты из {@link ActionParam#getKiasName}.
     * </li>
     * <li>
     * Если структура параметров глубокая (иерархическая), то имена параметров будут взяты из
     * {@link ActionParam#getPublicName}.
     * </li>
     * </ul>
     *
     * @param flatten возвращать плоскую (уплощенную) структуру или нет
     * @return the java.lang.String
     */
    @SuppressWarnings("unchecked")
    public String getValuesAsJson(boolean flatten) {
        Map<String, Object> justValuesCollection;
        if (flatten) {
            Map<String, Object> flatMap = getFlatMap();
            justValuesCollection = (Map<String, Object>) getJustValues(flatMap, flatten);
        } else {
            boolean isFlatStructure = isFlatStructure(this.structure);
            justValuesCollection = (Map<String, Object>) getJustValues(this.structure, isFlatStructure);
        }
        JSONObject jsonObject = new JSONObject(justValuesCollection);
        String result;
        try {
            result = jsonObject.toString(4);
        } catch (JSONException e) {
            Log.getLogger("main").error(null, e);
            result = "{}";
        }
        return result;
    }

    /**
     * Возвращает xml-фрагмент в виде строки, где листья - значения параметров.
     *
     * @param flatten возвращать плоскую (уплощенную) структуру или нет
     * @param rootName имя добавленного корневого элемента возвращаемого xml. Если rootName не передан, то возвращаемая
     *            строка является фрагментом xml, а не полноценным xml
     * @return xml-фрагмент в виде строки, где листья - значения параметров
     */
    @SuppressWarnings("unchecked")
    public String getValuesAsXml(boolean flatten, String... rootName) {
        Map<String, Object> justValuesCollection;
        if (flatten) {
            Map<String, Object> flatMap = getFlatMap();
            justValuesCollection = (Map<String, Object>) getJustValues(flatMap, flatten);
        } else {
            boolean isFlatStructure = isFlatStructure(this.structure);
            justValuesCollection = (Map<String, Object>) getJustValues(this.structure, isFlatStructure);
        }
        JSONObject jsonObject = new JSONObject(justValuesCollection);
        String result;
        try {
            result = XML.toString(jsonObject, (rootName.length > 0) ? rootName[0] : null);
        } catch (JSONException e) {
            Log.getLogger("main").error(null, e);
            result = Utils.getErrorMessage("", e.getMessage());
        }
        return result;

    }

    /**
     * Создает и возвращает новую структуру, где вместо листьев-ActionParam'ов только значения параметров.
     *
     * @param struct обрабатываемая структура
     * @param useKiasNames использовать в качестве имен параметров их значения {@link ActionParam#getKiasName()}
     * @return новую структуру, где вместо листьев-ActionParam'ов только значения параметров.
     */
    @SuppressWarnings("unchecked")
    private Object getJustValues(Object struct, boolean useKiasNames) {
        if (struct instanceof Map) {
            Map<String, Object> srcMap = (Map<String, Object>) struct;
            Map<String, Object> resultMap = Utils.newCaseInsensitiveMap();
            for (Map.Entry<String, Object> srcEntry : srcMap.entrySet()) {
                Object value = srcEntry.getValue();
                Object resultValue = getJustValues(value, useKiasNames);
                String resultKey = (value instanceof ActionParam)
                        ? (useKiasNames ? ((ActionParam) value).getKiasName() : srcEntry.getKey())
                        : srcEntry.getKey();
                resultMap.put(resultKey, resultValue);
            }
            return resultMap;

        } else if (struct instanceof List) {
            List<Object> srcList = (List<Object>) struct;
            int len = srcList.size();
            List<Object> resultList = new ArrayList<>(len);
            for (Object element : srcList) {
                Object resultValue = getJustValues(element, useKiasNames);
                resultList.add(resultValue);
            }
            return resultList;

        } else if (struct instanceof ActionParam) {
            ActionParam srcParam = (ActionParam) struct;
            Object result = srcParam.getValue();
            return result;

        } else {
            String result = String.valueOf(struct);
            return result;
        }
    }

    /**
     * Содержит ли коллекция массивы.
     * <br>
     * Считается, что в массивах содержатся подобные элементы, поэтому в коллекции с массивом не все элементы являются
     * уникальными по ключу.
     *
     * @return true, если коллекция на каком-либо уровне содержит массив.
     */
    public boolean isContainsArrays() {
        return this.containsArrays;
    }

    /**
     * Содержит ли коллекция массивы.
     *
     * @param containsArrays the containsArrays to set
     */
    private void setContainsArrays(boolean containsArrays) {
        this.containsArrays = containsArrays;
    }

    /**
     * Возвращает первый найденный ActionParam с заданным именем.
     *
     * @param name имя параметра
     * @return первый ActionParam с заданным именем. Если структура имеет массивы, то в ней допускается наличие
     *         нескольких параметров с одинаковым именем.
     */
    public ActionParam getParam(String name) {
        if (Utils.isEmpty(name)) {
            return null;
        }
        return getParam(name, structure);
    }

    /**
     * Возвращает первый найденный ActionParam с заданным именем.
     *
     * @param name имя параметра
     * @param struct структура, по которой ведется поиск
     * @return первый найденный ActionParam с заданным именем. Если структура имеет массивы, то в ней допускается
     *         наличие нескольких параметров с одинаковым именем.
     */
    @SuppressWarnings("unchecked")
    private ActionParam getParam(String name, Object struct) {
        ActionParam result = null;
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            for (Iterator<Object> iter = structMap.values().iterator(); result == null && iter.hasNext();) {
                Object value = iter.next();
                result = getParam(name, value);
            }
            return result;

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Iterator<Object> iter = structList.iterator(); result == null && iter.hasNext();) {
                Object element = iter.next();
                result = getParam(name, element);
            }
            return result;

        } else if (struct instanceof ActionParam) {
            ActionParam structParam = (ActionParam) struct;
            if (name.equalsIgnoreCase(structParam.getPublicName())) {
                // нашли нужный параметр, ради этого все и затевалось
                return structParam;
            }
            return null;

        } else {
            return null;
        }
    }

    /**
     * Возвращает строковое значение первого найденного конечного узла (листа) с заданным именем.
     * <br>
     * Если конечный узел - строка, метод вернет ее.
     * <br>
     * Если конечный узел - {@link ActionParam}, метод вернет {@link ActionParam#getValueAsString}.
     *
     * @param name имя узла
     * @return строковое значение первого найденного конечного узла (листа) с заданным именем. Если структура имеет
     *         массивы, то в ней допускается наличие нескольких параметров с одинаковым именем.
     */
    @Deprecated
    String getValue(String name) {
        if (Utils.isEmpty(name)) {
            return null;
        }
        return getValue(name, structure, 0);
    }

    /**
     * Возвращает строковое значение первого найденного конечного узла (листа) с заданным именем.
     * <br>
     * Если конечный узел - строка, метод вернет ее.
     * <br>
     * Если конечный узел - {@link ActionParam}, метод вернет {@link ActionParam#getValueAsString}.
     *
     * @param paramName имя параметра
     * @param struct структура, по которой ведется поиск
     * @param level уровень рекурсии
     * @return строковое значение первого найденного конечного узла (листа) с заданным именем. Если структура имеет
     *         массивы, то в ней допускается наличие нескольких параметров с одинаковым именем.
     */
    @SuppressWarnings("unchecked")
    private String getValue(String paramName, Object struct, int level) {
        String result = null;
        String[] split = paramName.split(SPLITTER);
        int len = split.length;
        String currentName = split[level];
        // String lastName = String.join(SPLITTER, Arrays.asList(split).subList(level, len));
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            if (structMap.containsKey(currentName)) {
                Object value = structMap.get(currentName);
                if (len - 1 == level) {
                    // находимся на нужном уровне структуры - не ищем подструктуру, ищем нужный узел в структуре
                    if ((value == null) || (value instanceof Map)) {
                        result = null;
                    } else if (value instanceof List) {
                        result = getValue(paramName, value, level);
                    } else if (value instanceof ActionParam) {
                        result = ((ActionParam) value).getValueAsString();
                    } else {
                        return value.toString();
                    }

                } else {
                    // ищем подстуктуру и спускаемся в нее
                    result = getValue(paramName, structMap.get(currentName), level + 1);
                }
            } else {
                result = null;
            }
            return result;

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Object element : structList) {
                result = getValue(paramName, element, level);
                if (result != null) {
                    break;
                }
            }
            return result;

        } else if (struct instanceof ActionParam) {
            ActionParam structParam = (ActionParam) struct;
            if (currentName.equalsIgnoreCase(structParam.getPublicName())) {
                // нашли нужный параметр, ради этого все и затевалось
                return structParam.getValueAsString();
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    /**
     * Добавляет в структуру переданный параметр.
     * <br>
     * Если в структуре под этим ключом лежит лист (параметр), он будет затерт.
     * <br>
     * Если в структуре под этим ключом лежит коллекция (map или список), будет выдана ошибка IllegalArgumentException.
     * <br>
     * Если в структуре на пути к этому листу лежит параметр (например, под ключом adc-def для добавляемого пути
     * abc-def-ghi), будет выдана ошибка IllegalArgumentException.
     * <br>
     * Если у переданного параметра {@link ActionParam#getValue()} = null, а в структуре под этим ключом уже лежит
     * какой-то объект (не коллекция, а примитив или ActionParam), то значение этого объекта будет присвоено
     * добавляемому параметру. Если же у переданного параметра {@link ActionParam#getValue()} не null, то оно меняться
     * не будет.
     *
     * @param param добавляемый в структуру параметр
     * @throws IllegalArgumentException если добавляемый в структуру параметр отсутствует
     * @return количество добавленных в структуру элементов
     */
    public int add(ActionParam param) throws IllegalArgumentException {
        if (param == null) {
            throw new IllegalArgumentException("param is null");
        }
        return add(param, this.structure, 0);
    }

    /**
     * Добавляет в структуру переданный параметр. Если у переданного параметра {@link ActionParam#getValue()} = null, а
     * в структуре под этим ключом уже лежит какой-то объект (не коллекция, а примитив или ActionParam), то значение
     * этого объекта будет присвоено добавляемому параметру. Если же у переданного параметра
     * {@link ActionParam#getValue()} не null, то оно меняться не будет.
     *
     * @param param добавляемый в структуру параметр.
     * @param struct обрабатываемая структура
     * @param level уровень рекурсии
     * @return количество добавленных в структуру элементов
     */
    @SuppressWarnings("unchecked")
    private int add(final ActionParam param, Object struct, int level) {
        int result = 0;
        String nodeName = param.getPublicName();
        String[] split = nodeName.split(SPLITTER);
        int len = split.length;
        String currentName = split[level];
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            if (len - 1 == level) {
                // находимся на нужном уровне структуры - не ищем подструктуру, вставляем переданный объект в структуру
                if (structMap.containsKey(currentName)) {
                    Object structValue = structMap.get(currentName);
                    if (structValue instanceof Map) {
                        // на месте вставляемого параметра уже есть коллекция - это не нормально
                        String fullName = String.join("/", Arrays.asList(split).subList(0, level + 1));
                        throw new IllegalArgumentException("Невозможно зарегистрировать узел " + nodeName
                                + " - в структуре на позиции " + fullName + " находится коллекция параметров");
                    } else if (structValue instanceof List) {
                        // на месте вставляемого параметра список - вставляем переданный объект в список
                        List<Object> structList = (List<Object>) structValue;
                        for (int i = 0, size = structList.size(); i < size; i++) {
                            Object element = structList.get(i);
                            if (element instanceof Map || element instanceof List) {
                                String fullName = String.join("/", Arrays.asList(split).subList(0, level));
                                throw new IllegalArgumentException("Невозможно зарегистрировать параметр " + nodeName
                                        + " - в структуре параметров на позиции " + fullName
                                        + " находится находится коллекция параметров");
                            } else if (element instanceof ActionParam) {
                                ActionParam newParam = new ActionParam(param);
                                if (newParam.getValue() == null) {
                                    ActionParam currentStructParam = (ActionParam) element;
                                    Object value = currentStructParam.getValue();
                                    newParam.setValue(value);
                                }
                                structList.set(i, newParam);
                                result++;
                            } else {
                                ActionParam newParam = new ActionParam(param);
                                newParam.setValue(element);
                                structList.set(i, newParam);
                                result++;
                            }
                        }
                    } else if (structValue instanceof ActionParam) {
                        // на месте вставляемого параметра уже есть параметр - заменяем его (возможно, оставляя его
                        // значение)
                        ActionParam newParam = new ActionParam(param);
                        if (newParam.getValue() == null) {
                            ActionParam currentStructParam = (ActionParam) structValue;
                            Object value = currentStructParam.getValue();
                            newParam.setValue(value);
                        }
                        structMap.put(currentName, newParam);
                        result++;
                    } else {
                        // на месте вставляемого параметра уже есть примитив - кладем на это место параметр (возможно,
                        // оставляя значение примитива)
                        ActionParam newParam = new ActionParam(param);
                        if (newParam.getValue() == null) {
                            newParam.setValue(structValue);
                        }
                        structMap.put(currentName, newParam);
                        result++;
                    }

                } else {
                    // под таким ключом ничего нет, просто кладем параметр в коллекцию
                    ActionParam newParam = new ActionParam(param);
                    structMap.put(currentName, newParam);
                    result++;
                }

            } else {
                // ищем (создаем) подстуктуру и спускаемся в нее
                if (!structMap.containsKey(currentName)) {
                    Map<String, Object> newSubstructure = Utils.newCaseInsensitiveMap();
                    structMap.put(currentName, newSubstructure);
                }
                result += add(param, structMap.get(currentName), level + 1);
            }
            return result;

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Object element : structList) {
                result += add(param, element, level);
            }
            return result;

        } else {
            String fullName = String.join("/", Arrays.asList(split).subList(0, level));
            throw new IllegalArgumentException(
                    "Невозможно зарегистрировать параметр " + nodeName + " - в структуре параметров на позиции "
                            + fullName + " находится конечный параметр, а не структура");
        }
    }

    /**
     * Добавляет в структуру значение value, размещая его под именем name.
     *
     * @param name имя размещаемого узла
     * @param value хранимое значение
     * @return количество добавленных в структуру элементов
     */
    private int add(String name, String value) {
        List<String> valuesList = new LinkedList<>();
        valuesList.add(value);
        return add(name, valuesList);
    }

    /**
     * Добавляет в структуру массив значений, размещая его под именем name.
     *
     * @param name имя размещаемого узла
     * @param values хранимые значения
     * @return количество добавленных в структуру элементов
     */
    private int add(String name, List<String> values) {
        List<NameValue> awList = new LinkedList<>();
        for (String value : values) {
            ActionParam notConfirmedParam = new ActionParam(name, name, value);
            notConfirmedParam.setConfirmed(false);
            ActionParamWrapper aw = new ActionParamWrapper(notConfirmedParam);
            awList.add(aw);
        }
        return add(awList, this.structure, 0);
    }

    /**
     * Добавяет в структуру массив пустых структур длины {@code listLength} под именем {@code name}. Если listLength=1,
     * то при конвертации структуры в формат xml массив станет обычным элементом.
     *
     * @param name имя, под которым будет размещен массив
     * @param listLength длина добавляемого массива. Если listLength=1, то при конвертации структуры в формат xml массив
     *            станет обычным элементом.
     * @return количество добавленных в структуру элементов
     */
    public int addList(String name, int listLength) {
        List<NameValue> awList = new LinkedList<>();

        LinkedList<Map<String, Object>> list = new LinkedList<>();
        for (int i = 1; i <= listLength; i++) {
            list.add(Utils.newCaseInsensitiveMap());
        }
        ObjectWrapper ow = new ObjectWrapper(name, list);
        awList.add(ow);

        return add(awList, this.structure, 0);
    }

    /**
     * Добавляет в структуру переданные объекты в качестве листов.
     * <br>
     * Если длина переданного списка равна 1, то в структуру будет добавлен один лист.
     * <br>
     * Если длина переданного списка больше 1, то в структуру будет добавлен список, а в этот список все переданные
     * объекты в качестве листов. Ожидается, что у всех элементов этого массива одинаковый {@link NameValue#getName()}.
     * <br>
     * Метод затирает то, что было в коллекции под этим ключом ранее.
     *
     * @param storedObjectsList список сохраняемых объектов. Ожидается, что у всех элементов этого массива одинаковый
     *            {@link NameValue#getName}.
     * @param struct обрабатываемая структура
     * @param level уровень рекурсии
     * @return количество добавленных в структуру элементов
     */
    @SuppressWarnings("unchecked")
    private int add(List<NameValue> storedObjectsList, Object struct, int level) {
        int result = 0;
        if (storedObjectsList == null || storedObjectsList.isEmpty()) {
            return result;
        }
        NameValue firstItem = storedObjectsList.get(0);
        if (firstItem == null) {
            return result;
        }
        String nodeName = firstItem.getName();
        String[] split = nodeName.split(SPLITTER);
        int len = split.length;
        String currentName = split[level];
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            if (len - 1 == level) {
                // находимся на нужном уровне структуры - не ищем подструктуру, вставляем переданный объект в структуру
                if (structMap.containsKey(currentName) && structMap.get(currentName) instanceof Map) {
                    // на месте вставляемого объекта уже есть коллекция - это не нормально
                    String fullName = String.join("/", Arrays.asList(split).subList(0, level + 1));
                    throw new IllegalArgumentException("Невозможно зарегистрировать узел " + nodeName
                            + " - в структуре на позиции " + fullName + " находится коллекция параметров");

                } else {
                    if (storedObjectsList.size() > 1) {
                        LinkedList<Object> storedValue = new LinkedList<>();
                        for (NameValue item : storedObjectsList) {
                            storedValue.add(item.getValue());
                            result++;
                        }
                        structMap.put(currentName, storedValue);
                    } else {
                        structMap.put(currentName, firstItem.getValue());
                        result++;
                    }
                }

            } else {
                // ищем (создаем) подстуктуру и спускаемся в нее
                if (!structMap.containsKey(currentName)) {
                    Map<String, Object> newSubstructure = Utils.newCaseInsensitiveMap();
                    structMap.put(currentName, newSubstructure);
                }
                result += add(storedObjectsList, structMap.get(currentName), level + 1);
            }
            return result;

        } else if (struct instanceof List) {
            List<Object> srcList = (List<Object>) struct;
            for (Object element : srcList) {
                result += add(storedObjectsList, element, level);
            }
            return result;

        } else {
            String fullName = String.join("/", Arrays.asList(split).subList(0, level));
            throw new IllegalArgumentException(
                    "Невозможно зарегистрировать параметр " + nodeName + " - в структуре параметров на позиции "
                            + fullName + " находится конечный параметр, а не структура");
        }
    }

    /**
     * Добавляет структуру в структуру.
     *
     * @param whereToAdd имя узла родительской структуры, в который будет добавлена новая структура
     * @param addedStructure добавляемая структура
     */
    public void addStructure(String whereToAdd, ParamStructure addedStructure) {
        addStructure(whereToAdd, addedStructure.structure, this.structure, 0);
    }

    /**
     * Добавляет структуру в структуру.
     *
     * @param whereToAdd имя узла родительской структуры, в который будет добавлена новая структура
     * @param addedStructure добавляемая структура
     * @param struct (рекурсивно) обрабатываемая структура
     * @param level текущий уровень вложенности структуры
     * @return
     */
    @SuppressWarnings("unchecked")
    private void addStructure(String whereToAdd, Map<String, Object> addedStructure, Object struct, int level) {
        String[] split = whereToAdd.split(SPLITTER);
        int len = split.length;
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            if (len == level) {
                // находимся на нужном уровне структуры - не ищем подструктуру, вставляем все элементы переданной
                // структуры addedStructure в структуру
                // сперва проверяем, что под такими ключами в хранимой структуре ничего не лежит
                for (Map.Entry<String, Object> addedEntry : addedStructure.entrySet()) {
                    if (structMap.containsKey(addedEntry.getKey())) {
                        String fullName = whereToAdd + SPLITTER + addedEntry.getKey();
                        throw new IllegalArgumentException("Невозможно вставить новый узел " + fullName
                                + " - в структуре параметров на этой позиции уже находится узел");
                    }
                }
                // вставляем все элементы переданной структуры addedStructure в структуру
                for (Map.Entry<String, Object> addedEntry : addedStructure.entrySet()) {
                    structMap.put(addedEntry.getKey(), addedEntry.getValue());
                }

            } else {
                String currentName = split[level];
                Object destination;
                if (!Utils.notEmpty(currentName)) {
                    destination = structMap;
                } else {
                    if (!structMap.containsKey(currentName)) {
                        structMap.put(currentName, Utils.newCaseInsensitiveMap());
                    }
                    destination = structMap.get(currentName);
                }
                addStructure(whereToAdd, addedStructure, destination, level + 1);
            }

        } else if (struct instanceof List) {
            List<Object> srcList = (List<Object>) struct;
            for (Object element : srcList) {
                addStructure(whereToAdd, addedStructure, element, level);
            }

        } else {
            String fullName = String.join("/", Arrays.asList(split).subList(0, level));
            throw new IllegalArgumentException(
                    "Невозможно вставить новую структуру " + whereToAdd + " - в структуре параметров на позиции "
                            + fullName + " находится конечный параметр, а не структура");
        }
    }

    /**
     * Проверяет параметры на корректность, возвращает список ошибок в параметрах.
     *
     * @return список ошибок в параметрах
     */
    public ErrorList check() {
        return check(structure);
    }

    /**
     * Проверяет все листья-ActionParam'ы, возвращает список ошибок в параметрах.
     *
     * @param struct структура, по которой ведется поиск
     * @return список ошибок в параметрах
     */
    @SuppressWarnings("unchecked")
    private ErrorList check(Object struct) {
        ErrorList result = new ErrorList();
        ParamChecker paramChecker = new ParamChecker();
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            for (Map.Entry<String, Object> srcEntry : structMap.entrySet()) {
                Object value = srcEntry.getValue();
                if (value instanceof ActionParam) {
                    ((ActionParam) value).setHash((String) structMap.get("hash"));
                }
                result.addAll(check(value));
            }
            return result;

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Object element : structList) {
                result.addAll(check(element));
            }
            return result;

        } else if (struct instanceof ActionParam) {
            ActionParam structParam = (ActionParam) struct;
            ErrorList checkResult = paramChecker.check(structParam);
            result.addAll(checkResult);
            return result;

        } else {
            return result;
        }
    }

    /**
     * Возвращает плоскую коллекцию (map) содержащую все хранимые структурой параметры илистроки. Ключами являются имена
     * параметров. Если структура содержит массивы, то последние прочитанные параметры затрут ранее сохраненные в
     * результирующую структуру.
     *
     * @return плоскую коллекцию (map) содержащую все хранимые структурой параметры
     */
    public Map<String, Object> getFlatMap() {
        return getFlatMap(structure, "");
    }

    /**
     * Возвращает плоскую коллекцию (map) содержащую все хранимые структурой параметры. Ключами являются имена
     * параметров. Если структура содержит массивы, то последние прочитанные параметры затрут ранее сохраненные в
     * результирующую структуру.
     *
     * @param struct структура, по которой ведется поиск параметров
     * @param parentPrefix рекурсивно заполняемое имя родительского элемента. Используется как префикс для формирования
     *            полного имени текущего элемента.
     * @return плоскую коллекцию (map) содержащую все хранимые структурой параметры
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getFlatMap(Object struct, String parentPrefix) {
        Map<String, Object> result = Utils.newCaseInsensitiveMap();
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            for (Map.Entry<String, Object> structEntry : structMap.entrySet()) {
                String structKey = structEntry.getKey();
                Object structValue = structEntry.getValue();
                if (!((structValue instanceof Map)
                        || (structValue instanceof List)
                        || (structValue instanceof ActionParam))) {
                    result.put(parentPrefix + structKey, structValue);
                } else {
                    result.putAll(getFlatMap(structValue, parentPrefix + structKey + SPLITTER));
                }
            }
            return result;

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Object element : structList) {
                result.putAll(getFlatMap(element, parentPrefix));
            }
            return result;

        } else if (struct instanceof ActionParam) {
            ActionParam structParam = (ActionParam) struct;
            result.put(structParam.getPublicName(), structParam);
            return result;

        } else {
            return result;
        }
    }

    /**
     * Удаляет из структуры узел.
     *
     * @param nodeName имя удаляемого узла
     * @return количество удаленных из структуры элементов
     */
    public List<Object> remove(String nodeName) {
        return remove(nodeName, this.structure, 0);
    }

    /**
     * Удаляет из структуры узел.
     *
     * @param nodeName имя удаляемого узла
     * @param struct обрабатываемая структура
     * @param level уровень рекурсии
     * @return количество удаленных из структуры элементов
     */
    @SuppressWarnings("unchecked")
    private List<Object> remove(String nodeName, Object struct, int level) {
        List<Object> result = new ArrayList<>();
        String[] split = nodeName.split(SPLITTER);
        int len = split.length;
        String currentName = split[level];
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            if (structMap.containsKey(currentName)) {
                if ((len - 1) == level) {
                    // находимся на нужном уровне структуры - не ищем подструктуру, ищем удаляемый узел в структуре
                    result.add(structMap.remove(currentName));

                } else {
                    // ищем подстуктуру и спускаемся в нее
                    List<Object> removed = remove(nodeName, structMap.get(currentName), level + 1);
                    result.addAll(removed.stream()
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList()));
                }
            }

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Object element : structList) {
                List<Object> removed = remove(nodeName, element, level);
                result.addAll(removed.stream()
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()));
            }

        }
        return result;
    }

    /**
     * Удаляет из структуры все неподтвержденные, то есть не найденные в таблице action_params, параметры (у которых
     * {@link ActionParam#isConfirmed()} = false).
     *
     * @return количество удаленных из структуры элементов
     */
    public int removeNonconfirmed() {
        return removeNonconfirmed(this.structure);
    }

    /**
     * Удаляет из структуры все неподтвержденные, то есть не найденные в таблице action_params, параметры (у которых
     * {@link ActionParam#isConfirmed()} = false).
     *
     * @param struct обрабатываемая структура
     * @return количество удаленных из структуры элементов
     */
    @SuppressWarnings("unchecked")
    private int removeNonconfirmed(Object struct) {
        int result = 0;
        if (struct instanceof Map) {
            Map<String, Object> structMap = (Map<String, Object>) struct;
            for (Iterator<Map.Entry<String, Object>> iter = structMap.entrySet().iterator(); iter.hasNext();) {
                Map.Entry<String, Object> structEntry = iter.next();
                Object value = structEntry.getValue();
                if (value instanceof Map || value instanceof List) {
                    result += removeNonconfirmed(value);
                } else if (value instanceof ActionParam) {
                    ActionParam ap = (ActionParam) value;
                    if (!ap.isConfirmed()) {
                        iter.remove();
                        result++;
                    }
                } else if (value instanceof String) {
                    iter.remove();
                    result++;
                }
            }

        } else if (struct instanceof List) {
            List<Object> structList = (List<Object>) struct;
            for (Iterator<Object> iter = structList.iterator(); iter.hasNext();) {
                Object element = iter.next();
                if (element instanceof Map || element instanceof List) {
                    result += removeNonconfirmed(element);

                } else if (element instanceof ActionParam) {
                    ActionParam ap = (ActionParam) element;
                    if (!ap.isConfirmed()) {
                        iter.remove();
                        result++;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Интерфейс для обертки объекта, хранимого в структуре.
     */
    private interface NameValue {
        /**
         * Имя объекта (под ним он хранится в структуре).
         *
         * @return Имя объекта
         */
        String getName();

        /**
         * Хранимый в структуре объект.
         *
         * @return Хранимый в структуре объект.
         */
        Object getValue();
    }

    /**
     * Обертка типа {@link NameValue} над объектом типа {@link ActionParam}.
     */
    private class ActionParamWrapper implements NameValue {
        private ActionParam param = null;

        /**
         * Обертка типа {@link NameValue} над объектом типа {@link ActionParam}.
         *
         * @param param обертываемый объект типа {@link ActionParam}.
         */
        ActionParamWrapper(ActionParam param) {
            this.param = param;
        }

        @Override
        public String getName() {
            return param.getPublicName();
        }

        @Override
        public Object getValue() {
            return param;
        }
    }

    /**
     * Обертка для простых строковых значений.
     */
    private class StringWrapper implements NameValue {
        private String name = null;
        private String value = null;

        /**
         * Обертка типа {@link NameValue} над строкой.
         *
         * @param name имя объекта (под ним он хранится в структуре).
         * @param value обертываемая строка.
         */
        StringWrapper(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public Object getValue() {
            return this.value;
        }
    }

    /**
     * Обертка для объектов.
     */
    private class ObjectWrapper implements NameValue {
        private String name = null;
        private Object value = null;

        /**
         * Обертка типа {@link NameValue} над объектом.
         *
         * @param name имя объекта (под ним он хранится в структуре).
         * @param value обертываемый объект.
         */
        ObjectWrapper(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public Object getValue() {
            return this.value;
        }
    }
}
