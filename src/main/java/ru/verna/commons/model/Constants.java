package ru.verna.commons.model;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Константы общего назначения.
 */
public final class Constants {

    /**
     * Имя хранимого обработчика Sid'а (хранится в контексте приложения).
     */
    public static final String SIDPROVIDER_ENTITYNAME = "SidProvider";
    /**
     * Имя хранимого пользователя (хранится в контексте запроса).
     */
    public static final String USER_ENTITYNAME = "TheUser";
    /**
     * Имя параметра username хранится в контексте запроса).
     */
    public static final String USERNAME_PARAMETER_NAME = "Name";
    /**
     * Имя параметра password (хранится в контексте запроса).
     */
    public static final String PASSWORD_PARAMETER_NAME = "Pwd";
    /**
     * Имя метода аутентификации в КИАС.
     */
    public static final String AUTH_METHOD_NAME = "Auth";
    /**
     * Имя параметра Sid.
     */
    public static final String SID_PARAMETER_NAME = "Sid";
    /**
     * Имя метода для проверки сессии.
     */
    public static final String CHECKSESSION_METHOD_NAME = "CheckSession";
    /**
     * JSON-конвертер.
     */
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    /**
     * Закрытый конструктор.
     */
    private Constants() {
    }

    /**
     * Коды ошибок хранятся в таблице errors.
     */
    public enum ERRORCODE {
        AUTH_TOKEN_NOT_FOUND(101), // Токен не найден
        USER_NOT_FOUND(102), // Пользователь не найден
        ACTION_IS_NOT_SET(103), // Действие не определено
        ACTION_IS_NOT_ALLOWED(104), // Действие не разрешено
        PARAMETER_REQUIRED(105), // Не заполнен обязательный параметр
        PARAMETER_MAXLENGTH_EXCEEDED(106), // превышена максимальная разрешенная длина значения
        PARAMETER_NOT_MATCH_PATTERN(107), // неверное значение (значение не соответствует нужному шаблону)
        PARAMETER_MINLENGTH_EXCEEDED(108), // превышена минимальная разрешенная длина значения
        AUTH_TOKEN_EXPIRED(109), // Токен просрочен
        WS_RSA_ERROR(110), // Ошибки методов РСА
        RULE_VIOLATION(120), // нарушено (бизнес-)правило
        EXCESS_OF_THE_AMOUNT(150), // превышена максимальная сумма оплаты он-лайн
        GATE_METHOD_ERROR(197), // общие ошибки в работе шлюза
        KIAS_WEB_METHOD_ERROR(198), // ошибки в работе веб методов КИАС
        DB_ERROR(199), // ошибки БД
        KIASSVC_IS_UNREACHABLE(500); // хост веб-службы КИАСа недоступен

        private final int code;

        /**
         * Конструктор.
         *
         * @param code код ошибки
         */
        ERRORCODE(int code) {
            this.code = code;
        }

        /**
         * Возвращает код ошибки.
         *
         * @return код ошибки
         */
        public int code() {
            return code;
        }
    }
}
