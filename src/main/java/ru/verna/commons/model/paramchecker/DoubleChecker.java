package ru.verna.commons.model.paramchecker;

import static ru.verna.commons.model.Constants.ERRORCODE.PARAMETER_NOT_MATCH_PATTERN;
import ru.verna.commons.Utils;
import ru.verna.commons.model.ActionParam;

/**
 * Проверка корректности значений параметров.
 */
public class DoubleChecker extends TypeChecker {

    @Override
    public ErrorList check(ActionParam param) {
        ErrorList result = new ErrorList();
        ParamError checkRequired = checkRequired(param);
        result.add(checkRequired);

        String valueAsString = param.getValueAsString();
        try {
            if (Utils.notEmpty(valueAsString)) {
                Double.parseDouble(valueAsString);
            }
        } catch (NumberFormatException nfe) {
            ParamError err = new ParamError(param, PARAMETER_NOT_MATCH_PATTERN.code(),
                    "значение должно быть числом с плавающей точкой");
            result.add(err);
        }

        ParamError checkLength = checkLength(param);
        result.add(checkLength);

        return result;
    }
}
