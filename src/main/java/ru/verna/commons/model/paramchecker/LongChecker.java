package ru.verna.commons.model.paramchecker;

import static ru.verna.commons.model.Constants.ERRORCODE.PARAMETER_NOT_MATCH_PATTERN;
import java.util.regex.Pattern;
import ru.verna.commons.Utils;
import ru.verna.commons.model.ActionParam;

/**
 * Проверка корректности значений параметров типа {@code long}.
 */
class LongChecker extends TypeChecker {

    @Override
    public ErrorList check(ActionParam param) {
        ErrorList result = new ErrorList();
        result.add(checkRequired(param));
        result.add(isNumber(param));

        Integer maxlength = param.getMaxlength();
        if ((maxlength == null) || (maxlength == 0)) {
            param.setMaxlength(String.valueOf(Long.MAX_VALUE).length());
        }

        result.add(checkLength(param));

        try {
            String valueAsString = param.getValueAsString();
            if (Utils.notEmpty(valueAsString)) {
                Long.parseLong(valueAsString);
            }
        } catch (NumberFormatException nfe) {
            ParamError err = new ParamError(param, PARAMETER_NOT_MATCH_PATTERN.code(),
                    "значение должно быть в пределах от " + Long.MIN_VALUE + " до "
                            + Long.MAX_VALUE);
            result.add(err);
        }

        return result;
    }

    /**
     * Проверяем содержит ли значение переданного параметра не цифры.
     *
     * @param param экземпляр проверяемого {@link ActionParam}
     * @return {@link ParamError#NO_ERROR} если в значении переданного параметра находятся только цифры иначе
     *         {@link ParamError} с описанием ошибки
     */
    private ParamError isNumber(ActionParam param) {
        ParamError paramError = ParamError.NO_ERROR;
        Pattern pattern = Pattern.compile("\\D");
        if (pattern.matcher(param.getValueAsString()).find()) {
            paramError = new ParamError(param, PARAMETER_NOT_MATCH_PATTERN.code(),
                    "значение должно содержать только цифры");
        }
        return paramError;
    }
}
