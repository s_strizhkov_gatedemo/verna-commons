package ru.verna.commons.model.paramchecker;

import ru.verna.commons.model.ActionParam;

/**
 * Все пропускает.
 */
public class KindChecker extends TypeChecker {
    @Override
    public ErrorList check(ActionParam param) {
        return NO_ERRORS;
    }

}
