package ru.verna.commons.model.paramchecker;

import ru.verna.commons.model.ActionParam;
import ru.verna.commons.model.Error;

/**
 * Ошибка в параметре действия.
 */
public class ParamError extends Error {
    /**
     * Null-объект для отсутствия ошибки.
     */
    public static final ParamError NO_ERROR = new ParamError(null, -1, "");

    private ActionParam param;

    /**
     * Конструктор.
     *
     * @param parameter параметр
     * @param code код ошибки
     * @param text текст ошибки
     */
    public ParamError(ActionParam parameter, int code, String text) {
        setParam(parameter);
        this.setCode(code);
        this.setText(text);
    }

    /**
     * @return the param
     */
    public ActionParam getParam() {
        return param;
    }

    /**
     * @param param the param to set
     */
    public void setParam(ActionParam param) {
        this.param = param;
    }

    /**
     * Возвращает описание ошибки обернутое в XML представление ошибок.
     *
     * @return XML строку описания ошибки
     */
    public String getDescription() {
        StringBuffer result = new StringBuffer();
        result.append("<error>\n")
                .append("<code>")
                .append(this.getCode())
                .append("</code>\n")
                .append("<parameter>")
                .append(this.getParam().getPublicName())
                .append("</parameter>\n")
                .append("<value>")
                .append(this.getParam().getValueAsString())
                .append("</value>\n")
                .append("<hash>")
                .append(this.getParam().getHash())
                .append("</hash>\n")
                .append("<text>")
                .append(this.getText())
                .append("</text>\n")
                .append("</error>\n");
        return result.toString();
    }
}
