package ru.verna.commons.model.paramchecker;

import ru.verna.commons.Utils;
import ru.verna.commons.model.ActionParam;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import static ru.verna.commons.model.Constants.ERRORCODE.PARAMETER_NOT_MATCH_PATTERN;

/**
 * Проверка корректности значений строковых параметров.
 */
class DateChecker extends TypeChecker {
    @Override
    public ErrorList check(ActionParam param) {
        ErrorList result = new ErrorList();
        ParamError checkRequired = checkRequired(param);
        result.add(checkRequired);

        String valueAsString = param.getValueAsString();
        Date value = null;
        try {
            if (Utils.notEmpty(valueAsString)) {
                Locale forLanguageTag = Locale.forLanguageTag("ru");
                DateFormat dateInstance = DateFormat.getDateInstance(DateFormat.MEDIUM, forLanguageTag);
                value = dateInstance.parse(valueAsString);
            }
        } catch (ParseException ex) {
            ParamError err = new ParamError(param, PARAMETER_NOT_MATCH_PATTERN.code(), "значение должно быть датой в формате DD.MM.YYYY");
            result.add(err);
        }

        return result;
    }

}
