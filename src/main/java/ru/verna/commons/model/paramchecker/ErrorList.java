package ru.verna.commons.model.paramchecker;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Список ошибок в переданных параметрах действий.
 */
public class ErrorList extends ArrayList<ParamError> {
    static final long serialVersionUID = 1L;

    /**
     * Возвращает стандартное сообщение об ошибках в параметрах.
     *
     * @return the java.lang.String
     */
    public String getErrorsMessage() {
        StringBuffer result = new StringBuffer("<data>\n");
        this.forEach(paramError -> result.append(paramError.getDescription()));
        result.append("</data>");
        return result.toString();
    }

    @Override
    public boolean add(ParamError e) {
        if (e != null && !ParamError.NO_ERROR.equals(e)) {
            return super.add(e);
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends ParamError> collection) {
        boolean result = false;
        for (ParamError next : collection) {
            result = add(next) || result;
        }
        return result;
    }

    @Override
    public void add(int index, ParamError element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends ParamError> c) {
        throw new UnsupportedOperationException();
    }

}
