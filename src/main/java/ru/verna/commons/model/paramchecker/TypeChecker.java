package ru.verna.commons.model.paramchecker;

import ru.verna.commons.model.ActionParam;
import static ru.verna.commons.model.Constants.ERRORCODE.*;

/**
 * Возвращает пустую строку, если параметр прошел проверку, иначе возвращает сообщение об ошибке.
 */
abstract class TypeChecker {
    /**
     * Пустая строка - стандартное сообщение об успешной проверке параметра.
     */
    protected static final ErrorList NO_ERRORS = new ErrorList();

    /**
     * Каждое отдельное сообщение об ошибке нужно завершать этой строкой.
     */
    protected static final String POSTFIX = "";

    /**
     * Возвращает пустую строку, если параметр прошел проверку, иначе возвращает сообщение об ошибке.
     *
     * @param param проверяемый параметр
     * @return пустую строку, если параметр прошел проверку, иначе возвращает сообщение об ошибке.
     */
    public abstract ErrorList check(ActionParam param);

    /**
     * Проверка обязательного параметра на заполненность.
     *
     * @param param проверяемый параметр
     * @return пустую строку, если параметр прошел проверку, или сообщение об ошибке.
     */
    protected ParamError checkRequired(ActionParam param) {
        String valueAsString = param.getValueAsString();
        boolean required = param.isRequired();
        if (required && "".equalsIgnoreCase(valueAsString)) {
            return new ParamError(param, PARAMETER_REQUIRED.code(), "не заполнен обязательный параметр");
        }
        return ParamError.NO_ERROR;
    }

    /**
     * Проверка строкового представления параметра на предельную длину.
     *
     * @param param проверяемый параметр
     * @return пустую строку, если параметр прошел проверку, или сообщение об ошибке.
     */
    protected ParamError checkLength(ActionParam param) {
        ParamError err = ParamError.NO_ERROR;
        String valueAsString = param.getValueAsString();
        Integer maxlength = param.getMaxlength();
        Integer minlength = param.getMinlength();
        if (maxlength != null && maxlength > 0 && valueAsString.length() > maxlength) {
            err = new ParamError(param, PARAMETER_MAXLENGTH_EXCEEDED.code(), "превышена разрешенная длина значения");
        } else if (minlength != null && minlength > 0 && valueAsString.length() < minlength) {
            err = new ParamError(param, PARAMETER_MINLENGTH_EXCEEDED.code(), "недостаточная длина значения");
        }
        return err;
    }
}
