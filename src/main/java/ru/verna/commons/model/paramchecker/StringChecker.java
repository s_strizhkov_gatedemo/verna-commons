package ru.verna.commons.model.paramchecker;

import ru.verna.commons.Utils;
import ru.verna.commons.model.ActionParam;
import static ru.verna.commons.model.Constants.ERRORCODE.PARAMETER_NOT_MATCH_PATTERN;

/**
 * Проверка корректности значений строковых параметров.
 */
class StringChecker extends TypeChecker {
    @Override
    public ErrorList check(ActionParam param) {
        ErrorList result = new ErrorList();
        ParamError checkRequired = checkRequired(param);
        result.add(checkRequired);

        ParamError checkLength = checkLength(param);
        result.add(checkLength);

        String value = param.getValueAsString();
        String mask = param.getMask();
        if (Utils.notEmpty(mask)) {
            if (checkRequired.equals(ParamError.NO_ERROR) && Utils.notEmpty(param.getValueAsString()) && !value.matches(mask)) {
                ParamError err = new ParamError(param, PARAMETER_NOT_MATCH_PATTERN.code(), "значение не соответствует требуемым условиям");
                result.add(err);
            }
        }

        return result;
    }
}
