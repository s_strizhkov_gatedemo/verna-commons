package ru.verna.commons.model.paramchecker;

import ru.verna.commons.model.ActionParam;

/**
 * Проверяет значение параметра на соответствие правилам.
 */
public class ParamChecker {

    /**
     * Проверяет значение параметра на соответствие правилам.
     *
     * @param param проверяемый параметр
     * @return Возвращает пустую строку, если параметр прошел проверку, иначе возвращает сообщение об ошибке.
     * @see TypeChecker#check(ActionParam)
     */
    public ErrorList check(ActionParam param) {
        String paramType = param.getTypeId();
        TypeChecker checker = getChecker(paramType);
        return checker.check(param);
    }

    /**
     * Выдает проверятор параметра в зависимости от его типа.
     *
     * @param type тип параметра, который надо проверить
     * @return проверятор параметра
     */
    TypeChecker getChecker(String type) {
        switch (type) {
            case "string":
                return new StringChecker();
            case "long":
                return new LongChecker();
            case "numeric":
                return new DoubleChecker();
            case "date":
                return new DateChecker();
            default:
                return new KindChecker();
        }
    }
}
