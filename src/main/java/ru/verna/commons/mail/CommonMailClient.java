package ru.verna.commons.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.LoggerFactory;
import ru.verna.commons.Utils;
import ru.verna.commons.log.Log;

/**
 * Класс создающий и настраивающий общий Mail клиент для работы приложения.
 */
public final class CommonMailClient {
    private static final String MAIL_PROPERTIES_FILE = "mail.properties";
    private static final Properties MAIL_PROPERTIES = new Properties();
    private static String user;
    private static String password;
    private static String from;
    private static String to;


    static {
        try (InputStream logResourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(MAIL_PROPERTIES_FILE)) {
            Log.getLogger("main").info("CommonMailClient reads {} file", MAIL_PROPERTIES_FILE);

            MAIL_PROPERTIES.load(logResourceAsStream);
            user = getProperty("gate.mail.user");
            password = getProperty("gate.mail.password");
            from = getProperty("gate.mail.from");
            to = getProperty("gate.mail.to");

            Log.getLogger("main").info("CommonMailClient initialized");

        } catch (IOException e) {
            Log.getLogger("main").error(null, e);
            user = "";
            password = "";
        }
    }

    /**
     * Приватный конструктор.
     */
    private CommonMailClient() {
    }

    /**
     * Отправка сообщения электронной почты на почту, заданную в настройках приложения.
     *
     * @param subject     тема сообщения
     * @param msg         текст сообщения
     */
    public static void sendMail(String subject, String msg) {
        sendMail(to, subject, msg);
    }

    /**
     * Отправка сообщения электронной почты.
     *
     * @param destination адрес куда отправлять
     * @param subject     тема сообщения
     * @param msg         текст сообщения
     */
    public static void sendMail(String destination, String subject, String msg) {
        if (Utils.notEmpty(destination)) {
            MimeMessage message = new MimeMessage(getMailSession());
            try {
                message.addHeader("Content-type", "text/HTML; charset=UTF-8");
                message.addHeader("format", "flowed");
                message.addHeader("Content-Transfer-Encoding", "8bit");
                message.setFrom(new InternetAddress(from));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(destination));

                message.setSubject(subject, "UTF-8");
                message.setText(msg, "UTF-8");
                message.setSentDate(new Date());
                LoggerFactory.getLogger("main").debug("Отправляем на адрес {} сообщение: [{}]", destination, msg);
                Transport.send(message);
            } catch (MessagingException e) {
                LoggerFactory.getLogger("main").error("Cannot send mail", e);
            }
        }
    }

    /**
     * Создание почтовой сессии.
     *
     * @return созданный экземпляр {@link Session}
     */
    private static Session getMailSession() {
        try {

            return Session.getInstance(MAIL_PROPERTIES, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, password);
                }
            });

        } catch (Exception e) {
            Log.getLogger("main").error(null, e);
            return Session.getDefaultInstance(new Properties());
        }
    }

    /**
     * Возвращает значение заданного свойства из файла настроек почты.
     * @param name имя свойства
     * @return значение свойства
     */
    public static String getProperty(String name) {
        return MAIL_PROPERTIES.getProperty(name);
    }
}
