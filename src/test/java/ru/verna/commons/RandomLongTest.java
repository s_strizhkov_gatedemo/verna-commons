package ru.verna.commons;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Проверка {@link Utils#getRandomLong(int...)}.
 */
public class RandomLongTest {

    private static final int REPEAT_COUNT = 100000;

    @Test
    public void noArguments() {
        for (int i = 1; i < REPEAT_COUNT; i++) {
            long result = Utils.getRandomLong();// по умолчанию размерность 9
            assertTrue((result >= 100000000) && (result <= (1000000000 - 1)), "result = " + result);
        }
    }

    @Test
    public void nullArgument() {
        for (int i = 1; i < REPEAT_COUNT; i++) {
            long result = Utils.getRandomLong(null); // по умолчанию размерность 9
            assertTrue((result >= 100000000) && (result <= (1000000000 - 1)), "result = " + result);
        }
    }

    @Test
    public void length4() {
        for (int i = 1; i < REPEAT_COUNT; i++) {
            long result = Utils.getRandomLong(4);
            assertTrue((result >= 1000) && (result <= 9999), "result = " + result);
        }
    }

    @Test
    public void length1() {
        for (int i = 1; i < REPEAT_COUNT; i++) {
            long result = Utils.getRandomLong(1);
            assertTrue((result >= 1) && (result <= 9), "result = " + result);
        }
    }
}
