package ru.verna.commons;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Тестирование метода {@link Utils#restrictString(java.lang.String, int...)}.
 */
public class RestrictStringTest {

    /**
     * Вызов без аргументов возвращает неизмененную строку.
     */
    @Test
    public void noArgs() {
        String src = "1234567890";
        String result = Utils.restrictString(src);
        assertEquals(src, result, src);
    }

    /**
     * Аргумент - пустой массив.
     */
    @Test
    public void array() {
        String src = "1234567890";
        String result = Utils.restrictString(src);
        assertEquals(src, result, src);
    }

    /**
     * Вызов с нулем возвращает неизмененную строку.
     */
    @Test
    public void withZero() {
        String src = "1234567890";
        String result = Utils.restrictString(src, 0);
        assertEquals(src, result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArg() {
        String src = "1234567890";
        String result = Utils.restrictString(src, 7);
        assertEquals("1234567...", result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArg2() {
        String src = "1234567890";
        String result = Utils.restrictString(src, 2);
        assertEquals("12...", result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void anotherPositiveArg() {
        String src = "1234567890";
        String result = Utils.restrictString(src, 9);
        assertEquals("123456789...", result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArgMoreThanLength() {
        String src = "1234567890";
        String result = Utils.restrictString(src, 14);
        assertEquals("1234567890", result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArgEqualLength() {
        String src = "1234567890";
        String result = Utils.restrictString(src, 10);
        assertEquals("1234567890", result, src);
    }

    /**
     * Вызов c отрицательным числом возвращает правую часть строки.
     */
    @Test
    public void negativeArg() {
        String src = "1234567890";
        String result = Utils.restrictString(src, -8);
        assertEquals("...34567890", result, src);
    }

    /**
     * Вызов c отрицательным числом возвращает правую часть строки.
     */
    @Test
    public void negativeArgMoreThanLength() {
        String src = "1234567890";
        String result = Utils.restrictString(src, -11);
        assertEquals("1234567890", result, src);
    }

    /**
     * Вызов c отрицательным числом возвращает правую часть строки.
     */
    @Test
    public void negativeArgEqualLength() {
        String src = "1234567890";
        String result = Utils.restrictString(src, -10);
        assertEquals("1234567890", result, src);
    }

    /**
     * Вызов без аргументов возвращает неизмененную строку.
     */
    @Test
    public void noArgsEmptyString() {
        String src = "";
        String result = Utils.restrictString(src);
        assertEquals(src, result, src);
    }

    /**
     * Вызов с нулем возвращает неизмененную строку.
     */
    @Test
    public void withZeroEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, 0);
        assertEquals(src, result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArgEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, 7);
        assertEquals("", result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArgMoreThanLengthEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, 14);
        assertEquals("", result, src);
    }

    /**
     * Вызов c положительным числом возвращает левую часть строки.
     */
    @Test
    public void positiveArgEqualLengthEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, 10);
        assertEquals("", result, src);
    }

    /**
     * Вызов c отрицательным числом возвращает правую часть строки.
     */
    @Test
    public void negativeArgEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, -8);
        assertEquals("", result, src);
    }

    /**
     * Вызов c отрицательным числом возвращает правую часть строки.
     */
    @Test
    public void negativeArgMoreThanLengthEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, -11);
        assertEquals("", result, src);
    }

    /**
     * Вызов c отрицательным числом возвращает правую часть строки.
     */
    @Test
    public void negativeArgEqualLengthEmptyString() {
        String src = "";
        String result = Utils.restrictString(src, -10);
        assertEquals("", result, src);
    }

    /**
     * Любоый вызов с null-строкой возвращает null.
     */
    @Test
    public void positiveArgNullString() {
        String src = null;
        String result = Utils.restrictString(src, 7);
        assertNull(src, result);
    }
}
