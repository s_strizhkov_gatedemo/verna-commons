package ru.verna.commons;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тестирование
 * {@link Utils#getSaltedMD5(java.lang.String)}, {@link Utils#md5(java.lang.String)}, {@link Utils#getSalt()}.
 */
public class MD5Test {

    @Test
    public void saltLength() {
        for (int i = 1; i < 100000; i++) {
            String salt = Utils.getSalt();
            assertEquals(4, salt.length(), "salt=" + salt);
        }
    }

    @Test
    public void test() {
        String password = "big password with !@#!@#$!#$ and spaces sdofijgrnkew,c o[sihtrsdfgoij' lcsjso9iu45oiw ckirotij +_()&# ";
        test(password);
    }

    @Test
    public void testNull() {
        String password = null;
        test(password);
    }

    @Test
    public void testEmptyString() {
        String password = "";
        test(password);
    }

    private void test(String password) {
        String saltedMD5 = Utils.getSaltedMD5(password);

        int magicallyKnownSaltLength = 4;
        String salt = saltedMD5.substring(0, magicallyKnownSaltLength);
        String md5 = Utils.md5(salt + password);

        assertEquals(saltedMD5, salt + md5);
    }
}
