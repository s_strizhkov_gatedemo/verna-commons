package ru.verna.commons;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogTest {

    @Test
    public void filterLog() throws IOException {
        String rawLog = "{\n"
                + "    \"status\": 200,\n"
                + "    \"contentType\": \"application/json;charset=UTF-8\",\n"
                + "    \"headers\": {\n"
                + "        \"Server\": \"GlassFish Server Open Source Edition  4.1.2 \",\n"
                + "        \"X-Powered-By\": \"Servlet/3.1 JSP/2.3 (GlassFish Server Open Source Edition  4.1.2  Java/Oracle Corporation/1.8)\"\n"
                + "    },\n"
                + "    \"body\": \"{'data':{'result':{'row':[{'ParentISN':'1326771','ACTIVE':'N','ISN':'1329021','N_KIDS':'0','UPDATED':'01.06.2018','RefClassISN3':'','ShortName':'В РАБОЧЕЕ ВРЕМЯ','Code':'','RefClassISN2':'','RefClassISN1':'','TableName':'','ConstName':'','FullName':'В рабочее время','UserConstName':'ucDictiNSCoverageTermWorkTime','Level':'1','UPDATEDBY':'31468022','NumCode':''},{'ParentISN':'1326771','ACTIVE':'N','ISN':'1329031','N_KIDS':'0','UPDATED':'01.06.2018','RefClassISN3':'','ShortName':'В РАБОЧЕЕ ВРЕМЯ И ВО ВРЕМЯ ДОРОГИ НА РАБ','Code':'','RefClassISN2':'','RefClassISN1':'','TableName':'','ConstName':'','FullName':'В рабочее время и во время дороги на работу и с работы','UserConstName':'ucDictiNSCoverageTermWorkAndWalkTime','Level':'1','UPDATEDBY':'31468022','NumCode':''},{'ParentISN':'1326771','ACTIVE':'N','ISN':'1329051','N_KIDS':'0','UPDATED':'01.06.2018','RefClassISN3':'','ShortName':'ВО ВРЕМЯ УЧАСТИЯ В СПОРТИВНЫХ МЕРОПРИЯТИ','Code':'','RefClassISN2':'','RefClassISN1':'','TableName':'','ConstName':'','FullName':'Во...\"\n"
                + "}\n";
        DocumentContext documentContext = JsonPath.parse(rawLog);

        Properties properties = new Properties();
        InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("log.properties");
        properties.load(resourceAsStream);

        Utils.LOG_PROPERTIES.forEach((key, value) -> {
            if (!Boolean.parseBoolean((String) value)) {
                documentContext.delete("$." + key);
            }
        });

        String expectedString = "{\"status\":200,\"contentType\":\"application/json;charset=UTF-8\",\"headers\":{},\"body\":\"{'data':{'result':{'row':[{'ParentISN':'1326771','ACTIVE':'N','ISN':'1329021','N_KIDS':'0','UPDATED':'01.06.2018','RefClassISN3':'','ShortName':'В РАБОЧЕЕ ВРЕМЯ','Code':'','RefClassISN2':'','RefClassISN1':'','TableName':'','ConstName':'','FullName':'В рабочее время','UserConstName':'ucDictiNSCoverageTermWorkTime','Level':'1','UPDATEDBY':'31468022','NumCode':''},{'ParentISN':'1326771','ACTIVE':'N','ISN':'1329031','N_KIDS':'0','UPDATED':'01.06.2018','RefClassISN3':'','ShortName':'В РАБОЧЕЕ ВРЕМЯ И ВО ВРЕМЯ ДОРОГИ НА РАБ','Code':'','RefClassISN2':'','RefClassISN1':'','TableName':'','ConstName':'','FullName':'В рабочее время и во время дороги на работу и с работы','UserConstName':'ucDictiNSCoverageTermWorkAndWalkTime','Level':'1','UPDATEDBY':'31468022','NumCode':''},{'ParentISN':'1326771','ACTIVE':'N','ISN':'1329051','N_KIDS':'0','UPDATED':'01.06.2018','RefClassISN3':'','ShortName':'ВО ВРЕМЯ УЧАСТИЯ В СПОРТИВНЫХ МЕРОПРИЯТИ','Code':'','RefClassISN2':'','RefClassISN1':'','TableName':'','ConstName':'','FullName':'Во...\"}";

        assertEquals(expectedString, documentContext.jsonString());
    }
}
