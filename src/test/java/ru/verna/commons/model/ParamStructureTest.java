package ru.verna.commons.model;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.verna.commons.log.Log;
import ru.verna.commons.model.paramchecker.ErrorList;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
class ParamStructureTest {

    private String entryJSON = null;

    @BeforeEach
    void setUp() {
        entryJSON = "{\n"
                + "	'insurer': {\n"
                + "		'phone': '+7 938 111-11-11',\n"
                + "		'email': 'verna@test.ru',\n"
                + "		'surname': 'Иванов',\n"
                + "		'name': 'Иван',\n"
                + "		'patronymic': 'Иванович',\n"
                + "		'birthdate': '01.01.1980',\n"
                + "		'gender': 'Мужской',\n"
                + "		'doc': {\n"
                + "			'type': 'Паспорт РФ',\n"
                + "			'series': '0715',\n"
                + "			'number': '111111',\n"
                + "			'issued_by': 'УФМС',\n"
                + "			'date': '02.01.1998'\n"
                + "		},\n"
                + "		'address': {\n"
                + "			'city': 'Москва',\n"
                + "			'street': 'Тверская',\n"
                + "			'home': '1',\n"
                + "			'housing': '',\n"
                + "			'apartment': ''\n"
                + "		}\n"
                + "	},\n"
                + "	'insurant': [{\n"
                + "		'surname': 'Иванов',\n"
                + "		'name': 'Иван ',\n"
                + "		'patronymic': 'Иванович',\n"
                + "		'birthdate': '03.05.2018',\n"
                + "		'gender': 'Мужской',\n"
                + "		'doc': {\n"
                + "			'type': 'Паспорт РФ',\n"
                + "			'series': '1111',\n"
                + "			'number': '111111',\n"
                + "			'issued_by': 'УФМС',\n"
                + "			'date': '01.01.1998'\n"
                + "		},\n"
                + "		'address': {\n"
                + "			'city': 'Москва',\n"
                + "			'street': 'Тверская',\n"
                + "			'home': '10',\n"
                + "			'housing': '10',\n"
                + "			'apartment': '100'\n"
                + "		}\n"
                + "	},\n"
                + "	{\n"
                + "		'surname': 'Петров',\n"
                + "		'name': 'Петр',\n"
                + "		'patronymic': 'Петрович',\n"
                + "		'birthday': '03.05.2018',\n"
                + "		'gender': 'Мужской',\n"
                + "		'doc': {\n"
                + "			'type': 'Паспорт РФ',\n"
                + "			'series': '2222',\n"
                + "			'number': '222222',\n"
                + "			'issued_by': 'УФМС',\n"
                + "			'date': '02.02.1922'\n"
                + "		},\n"
                + "		'address': {\n"
                + "			'city': 'Москва',\n"
                + "			'street': 'Тверская',\n"
                + "			'home': '1000',\n"
                + "			'housing': '1000',\n"
                + "			'apartment': '10000'\n"
                + "		}\n"
                + "	}],\n"
                + "	'amount_per_insured': '160000',\n"
                + "	'count': 2,\n"
                + "	'date_policy_begin': '30.05.2018',\n"
                + "	'date_policy_end': '31.05.2018',\n"
                + "	'coverage_term': '24hours',\n"
                + "	'coverage_territory': 'РФ',\n"
                + "	'promo_code': '',\n"
                + "	'anotherarray': [\n"
                + "'qwe',\n"
                + "'asd',\n"
                + "'zxc',\n"
                + "{'bnm':'tyui'},\n"
                + "{'mnb':'iuyt'},\n"
                + "],\n"
                + "  }"
                + "}";
    }

    /**
     * Test of readJSON method, of class ParamStructure.
     *
     * @throws java.lang.Exception
     */
    @Test
    @SuppressWarnings("unchecked")
    void testReadJSON() throws Exception {
        ParamStructure instance = new ParamStructure();
        instance.readJSON(entryJSON);

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(10, structure.size());
        Assertions.assertTrue(structure.containsKey("insurer"));
        Assertions.assertTrue(structure.get("insurer") instanceof Map);

        Map<String, Object> substruct = (Map<String, Object>) structure.get("insurer");
        Assertions.assertEquals(9, substruct.size());
        Assertions.assertTrue(substruct.containsKey("address"));
        Assertions.assertTrue(substruct.get("address") instanceof Map);

        Map<String, Object> substruct2 = (Map<String, Object>) substruct.get("address");
        Assertions.assertEquals(5, substruct2.size());
        Assertions.assertTrue(substruct2.containsKey("city"));
        Assertions.assertTrue(substruct2.containsKey("street"));

        Assertions.assertEquals("String", substruct2.get("city").getClass().getSimpleName());
        Assertions.assertEquals("String", substruct2.get("street").getClass().getSimpleName());

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Добавление параметра в пустую структуру.
     */
    @Test
    @SuppressWarnings("unchecked")
    void testAddToEmpty() {
        ParamStructure instance = new ParamStructure();
        ActionParam param = new ActionParam("abc-def-ghi", "abc_def_ghi", "param value");

        int result = instance.add(param);
        Assertions.assertEquals(1, result);

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(1, structure.size());
        Assertions.assertTrue(structure.containsKey("abc"));
        Object abc = structure.get("abc");
        Assertions.assertTrue(abc instanceof Map);

        Map<String, Object> substruct = (Map<String, Object>) abc;
        Assertions.assertEquals(1, substruct.size());
        Assertions.assertTrue(substruct.containsKey("def"));
        Object def = substruct.get("def");
        Assertions.assertTrue(def instanceof Map);

        Map<String, Object> substruct2 = (Map<String, Object>) def;
        Assertions.assertEquals(1, substruct2.size());
        Assertions.assertTrue(substruct2.containsKey("ghi"));
        Object ghi = substruct2.get("ghi");
        Assertions.assertTrue(ghi instanceof ActionParam);
        Assertions.assertEquals("ActionParam", ghi.getClass().getSimpleName());
    }

    /**
     * Добавление нового параметра не ломает существующую структуру.
     */
    @Test
    @SuppressWarnings("unchecked")
    void testAddSecond() {
        ParamStructure instance = new ParamStructure();
        ActionParam param1 = new ActionParam("insurer-address-city", "insurer_address_city", "Краснодар");
        instance.add(param1);
        ActionParam param2 = new ActionParam("insurer-address-street", "insurer_address_street", "Красная");

        int result = instance.add(param2);
        Assertions.assertEquals(1, result);

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(1, structure.size());
        Assertions.assertTrue(structure.containsKey("insurer"));
        Object insurer = structure.get("insurer");
        Assertions.assertTrue(insurer instanceof Map);

        Map<String, Object> substruct = (Map<String, Object>) insurer;
        Assertions.assertEquals(1, substruct.size());
        Assertions.assertTrue(substruct.containsKey("address"));
        Object address = substruct.get("address");
        Assertions.assertTrue(address instanceof Map);

        Map<String, Object> substruct2 = (Map<String, Object>) address;
        Assertions.assertEquals(2, substruct2.size());
        Assertions.assertTrue(substruct2.containsKey("city"));
        Assertions.assertTrue(substruct2.containsKey("street"));

        Assertions.assertEquals("ActionParam", substruct2.get("city").getClass().getSimpleName());
        Assertions.assertEquals("ActionParam", substruct2.get("street").getClass().getSimpleName());

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Добавление нового параметра не ломает существующую структуру.
     */
    @Test
    void testAddToFlat() {
        int result;
        ParamStructure instance = new ParamStructure();
        ActionParam param1 = new ActionParam("param1", "p1", "строка");
        result = instance.add(param1);
        Assertions.assertEquals(1, result);

        ActionParam param2 = new ActionParam("param2", "p2", null);
        result = instance.add(param2);
        Assertions.assertEquals(1, result);

        ActionParam param3 = new ActionParam("param3", "p2", "строка3");
        result = instance.add(param3);
        Assertions.assertEquals(1, result);

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(3, structure.size());

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Новый добавленный параметр затирает существующий параметр.
     */
    @Test
    @SuppressWarnings("unchecked")
    void testAddReplace() {
        ParamStructure instance = new ParamStructure();
        ActionParam param1 = new ActionParam("insurer-address-city", "insurer_address_city", "Краснодар");
        instance.add(param1);
        ActionParam param2 = new ActionParam("insurer-address-street", "insurer_address_street", "Красная");
        instance.add(param2);

        ActionParam param3 = new ActionParam("insurer-address-street", "insurer_address_street", "Зеленая");

        int result = instance.add(param3);
        Assertions.assertEquals(1, result);

        ActionParam streetParam = instance.getParam("insurer-address-street");
        Assertions.assertEquals("Зеленая", streetParam.getValue());

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(1, structure.size());
        Assertions.assertTrue(structure.containsKey("insurer"));
        Object insurer = structure.get("insurer");
        Assertions.assertTrue(insurer instanceof Map);

        Map<String, Object> substruct = (Map<String, Object>) insurer;
        Assertions.assertEquals(1, substruct.size());
        Assertions.assertTrue(substruct.containsKey("address"));
        Object address = substruct.get("address");
        Assertions.assertTrue(address instanceof Map);

        Map<String, Object> substruct2 = (Map<String, Object>) address;
        Assertions.assertEquals(2, substruct2.size());
        Assertions.assertTrue(substruct2.containsKey("city"));
        Assertions.assertTrue(substruct2.containsKey("street"));

        Assertions.assertEquals("ActionParam", substruct2.get("city").getClass().getSimpleName());
        Assertions.assertEquals("ActionParam", substruct2.get("street").getClass().getSimpleName());

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Новый добавленный параметр затирает существующий параметр.
     */
    @Test
    @SuppressWarnings("unchecked")
    void testAddReplace2() {
        String x = "{'insurer': {'address': {\n"
                + "    'city': 'Краснодар',\n"
                + "    'street': 'Красная'\n"
                + "}}}\n"
                + "\n";

        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Logger.getLogger(ParamStructureTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        ActionParam param3 = new ActionParam("insurer-address-street", "insurer_address_street", "Зеленая");
        int result = instance.add(param3);
        Assertions.assertEquals(1, result);

        ActionParam streetParam = instance.getParam("insurer-address-street");
        Assertions.assertEquals("Зеленая", streetParam.getValue());

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(1, structure.size());
        Assertions.assertTrue(structure.containsKey("insurer"));
        Assertions.assertTrue(structure.get("insurer") instanceof Map);

        Map<String, Object> substruct = (Map<String, Object>) structure.get("insurer");
        Assertions.assertEquals(1, substruct.size());
        Assertions.assertTrue(substruct.containsKey("address"));
        Assertions.assertTrue(substruct.get("address") instanceof Map);

        Map<String, Object> substruct2 = (Map<String, Object>) substruct.get("address");
        Assertions.assertEquals(2, substruct2.size());
        Assertions.assertTrue(substruct2.containsKey("city"));
        Assertions.assertTrue(substruct2.containsKey("street"));

        Assertions.assertEquals("String", substruct2.get("city").getClass().getSimpleName());
        Assertions.assertEquals("ActionParam", substruct2.get("street").getClass().getSimpleName());

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Добавление параметра в массив приводит к добавлению параметра в каждый элемент массива.
     */
    @Test
    void testAddToArrays() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'insurer_address_street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'insurer_address_street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'insurer_address_street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        ActionParam param3 = new ActionParam("insurer-address-дом", "insurer_address_дом", "100");
        int result = instance.add(param3);
        Assertions.assertEquals(3, result);

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Добавление параметра в массив приводит к добавлению параметра в каждый элемент массива.
     */
    @Test
    void testDoubleAddToArrays() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'insurer_address_street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'insurer_address_street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'insurer_address_street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        ActionParam param3 = new ActionParam("insurer-address-дом-подъезд", "insurer_address_дом_подъезд", "2");
        int result = instance.add(param3);
        Assertions.assertEquals(3, result);

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);

        ActionParam param4 = new ActionParam("insurer-address-дом-этаж", "insurer_address_дом_этаж", "5");
        int result2 = instance.add(param4);
        Assertions.assertEquals(3, result2);

        asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Добавление параметра рядом с массивом не приводит к добавлению параметра в каждый элемент массива.
     */
    @Test
    void testAddToArrays2() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'insurer_address_street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'insurer_address_street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'insurer_address_street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        ActionParam param3 = new ActionParam("insurer-document-дом", "insurer_address_дом", "100");
        int result = instance.add(param3);
        Assertions.assertEquals(3, result);

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Добавление параметра рядом с массивом не приводит к добавлению параметра в каждый элемент массива.
     */
    @Test
    void testAddToNestedArrays() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'insurer_address_street': 'Красная',\n"
                + "                'flats': [\n"
                + "                    {'house': 100,\n"
                + "                     'подъезд': 2\n"
                + "                    },\n"
                + "                    {'house': 200,\n"
                + "                     'подъезд': 3\n"
                + "                    },\n"
                + "                    {'house': 300,\n"
                + "                     'подъезд': 4\n"
                + "                    },\n"
                + "                    {'house': 400,\n"
                + "                     'подъезд': 5\n"
                + "                    }\n"
                + "                ]\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'insurer_address_street': 'Тверская',\n"
                + "                'flats': [\n"
                + "                    {'house': 100,\n"
                + "                     'подъезд': 2\n"
                + "                    },\n"
                + "                    {'house': 200,\n"
                + "                     'подъезд': 3\n"
                + "                    },\n"
                + "                    {'house': 300,\n"
                + "                     'подъезд': 4\n"
                + "                    },\n"
                + "                    {'house': 400,\n"
                + "                     'подъезд': 5\n"
                + "                    }\n"
                + "                ]\n"
                + "\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'insurer_address_street': 'Schwarzenegger avenue',\n"
                + "                'flats': [\n"
                + "                    {'house': 100,\n"
                + "                     'подъезд': 2\n"
                + "                    },\n"
                + "                    {'house': 200,\n"
                + "                     'подъезд': 3\n"
                + "                    },\n"
                + "                    {'house': 300,\n"
                + "                     'подъезд': 4\n"
                + "                    },\n"
                + "                    {'house': 400,\n"
                + "                     'подъезд': 5\n"
                + "                    }\n"
                + "                ]\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        ActionParam param3 = new ActionParam("insurer-address-flats-квартира", "_", "100");
        int result = instance.add(param3);
        // 3 insurer * 4 flats в каждом = 12 добавленных параметров
        Assertions.assertEquals(12, result);

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    /**
     * Попытка положить параметр, если на этом месте уже лежит структура, приводит к ошибке IllegalArgumentException.
     */
    @Test
    void testAddWrongReplacingOfStructure() {
        ParamStructure instance = new ParamStructure();
        instance.add(new ActionParam("insurer-address-city", "insurer_address_city", "Краснодар"));
        ActionParam param2 = new ActionParam("insurer-address", "insurer_address", "Краснодар, ул. Красная, 100");

        Throwable t = null;
        try {
            int result = instance.add(param2);
            // должна вылететь ошибка, этот код должен быть недостижим
            Log.TEST.info("проблема: параметр добавлен, " + result);
        } catch (IllegalArgumentException e) {
            t = e;
        }
        Assertions.assertNotNull(t);
        Assertions.assertEquals("java.lang.IllegalArgumentException", t.getClass().getName());
        Log.TEST.info(t.getMessage());

    }

    /**
     * Попытка создать структуру, если на этом месте уже лежит конечный параметр, приводит к ошибке
     * IllegalArgumentException.
     */
    @Test
    @SuppressWarnings("unchecked")
    void testAddWrongReplacingOfParam() {
        ParamStructure instance = new ParamStructure();
        instance.add(new ActionParam("insurer-address", "insurer_address", "Краснодар, Красная, 100"));

        Throwable t = null;
        try {
            int result = instance.add(new ActionParam("insurer-address-street", "insurer_address_street", "Красная"));
            // должна вылететь ошибка, этот код должен быть недостижим
            Log.TEST.info("проблема: параметр добавлен, " + result);
        } catch (IllegalArgumentException e) {
            t = e;
        }
        Assertions.assertNotNull(t);
        Assertions.assertEquals("java.lang.IllegalArgumentException", t.getClass().getName());
        Log.TEST.info(t.getMessage());

        String valuesAsJson = instance.getValuesAsJson(false);
        Log.TEST.info(valuesAsJson);

        Map<String, Object> structure = instance.getStructure();
        Assertions.assertEquals(1, structure.size());
        Assertions.assertTrue(structure.containsKey("insurer"));
        Object insurer = structure.get("insurer");
        Assertions.assertTrue(insurer instanceof Map);

        Map<String, Object> substruct = (Map<String, Object>) insurer;
        Assertions.assertEquals(1, substruct.size());
        Assertions.assertTrue(substruct.containsKey("address"));
        Assertions.assertTrue(substruct.get("address") instanceof ActionParam);
    }

    /**
     * Попытка положить параметр, если на этом месте уже лежит структура, приводит к ошибке IllegalArgumentException.
     */
    @Test
    void testGetFlatMap() {
        String x = "{\n"
                + "    'param1': 'value 1',\n"
                + "    param2: value2,\n"
                + "    'insurant': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        Map<String, Object> flatMap = instance.getFlatMap();
        Assertions.assertEquals(4, flatMap.size());

        instance.add(new ActionParam("insurer-address-city", "insurer_address_city", "Краснодар"));
        instance.add(new ActionParam("insurer-address-street", "insurer_address_street", "Красная"));
        instance.add(new ActionParam("insurer-name", "insurer_name", "Иван"));
        instance.add(new ActionParam("insurer-doc-type", "insurer_doc_type", "Паспорт"));
        instance.add(new ActionParam("insurer-doc-number", "insurer_doc_number", "123456"));
        instance.add(new ActionParam("insurer-surname", "insurer_surname", "Иванов"));

        try {
            String asJson = instance.describeAsJson();
            Log.TEST.info("describeAsJson(): {}", asJson);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        flatMap = instance.getFlatMap();
        Assertions.assertEquals(10, flatMap.size());
        Log.TEST.info(String.valueOf(flatMap));

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    @Test
    void testGetParam() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        instance.add(new ActionParam("insurant-address-city", "insurer_address_city", "Краснодар"));
        instance.add(new ActionParam("insurant-address-street", "insurer_address_street", "Красная"));
        instance.add(new ActionParam("insurant-name", "insurer_name", "Иван"));
        instance.add(new ActionParam("insurant-doc-type", "insurer_doc_type", "Паспорт"));
        instance.add(new ActionParam("insurant-doc-number", "insurer_doc_number", "123456"));
        instance.add(new ActionParam("insurant-surname", "insurer_surname", "Иванов"));

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);

        ActionParam notExisting = instance.getParam("insurant-doc");
        Assertions.assertNull(notExisting);

        ActionParam idn = instance.getParam("insurant-doc-number");
        Assertions.assertEquals("123456", idn.getValue());

        // сейчас в массиве insurer только строки, параметров нет, поэтому вернет null
        ActionParam fromArray = instance.getParam("insurer-address-street");
        Assertions.assertNull(fromArray);

        instance.add(new ActionParam("insurer-address-street", "insurer_address_street", "Улица"));
        ActionParam fromArray2 = instance.getParam("insurer-address-street");
        Assertions.assertEquals("Улица", fromArray2.getValue());

        asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    @Test
    @SuppressWarnings({ "unchecked", "deprecation" })
    void testGetValue() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ],\n"
                + "    'param1': 'value 1',\n"
                + "    'param2': 'value 2',\n"
                + "    'param3': {\n"
                + "        'param31': {\n"
                + "            'param311': 'value 311',\n"
                + "            'param312': 'value 312'\n"
                + "        }\n"
                + "    },\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        instance.add(new ActionParam("insurant-address-city", "insurer_address_city", "Краснодар"));
        instance.add(new ActionParam("insurant-address-street", "insurer_address_street", "Красная"));
        instance.add(new ActionParam("insurant-name", "insurer_name", "Иван"));
        instance.add(new ActionParam("insurant-doc-type", "insurer_doc_type", "Паспорт"));
        instance.add(new ActionParam("insurant-doc-number", "insurer_doc_number", "123456"));
        instance.add(new ActionParam("insurant-surname", "insurer_surname", "Иванов"));

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);

        // чтение несуществующего параметра
        String notExisting = instance.getValue("insurant-doc");
        Assertions.assertNull(notExisting);

        // чтение существующего параметра
        String idn = instance.getValue("insurant-doc-number");
        Assertions.assertEquals("123456", idn);

        // чтение значения (строки) из массива
        String fromArray = instance.getValue("insurer-address-street");
        Log.TEST.info(fromArray);
        Assertions.assertNotNull(fromArray);

        // перезатираем все street в массиве insurer
        instance.add(new ActionParam("insurer-address-street", "insurer_address_street", "Улица"));
        String fromArray2 = instance.getValue("insurer-address-street");
        Assertions.assertEquals("Улица", fromArray2);

        // чтение значения (строки)
        String p1 = instance.getValue("param1");
        Assertions.assertEquals("value 1", p1);

        // есть узел param3-param31-param312, а узла param312 нет
        String p312 = instance.getValue("param312");
        Assertions.assertNull(p312);

        String deep312 = instance.getValue("param3-param31-param312");
        Assertions.assertEquals("value 312", deep312);

        String p3 = instance.getValue("param3");
        Assertions.assertNull(p3);

        asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    @Test
    void testCheck() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        ActionParam p = new ActionParam("insurant-phone", "insurer_phone", "Краснодар");
        p.setDescr("Телефон");
        p.setMask("^\\+7\\-\\d{3}\\-\\d{3}\\-\\d{2}\\-\\d{2}$");
        p.setTypeId("string");
        p.setMaxlength(5);
        instance.add(p);

        ActionParam p2 = new ActionParam("insurant-doc-number", "insurer_dn", "876543");
        p2.setDescr("Номер паспорта");
        p2.setTypeId("long");
        p2.setMaxlength(6);
        p2.setMinlength(6);
        instance.add(p2);

        ActionParam p3 = new ActionParam("insurant-doc-series", "insurer_ds", "строка");
        p3.setDescr("Серия паспорта");
        p3.setTypeId("long");
        p3.setMaxlength(6);
        instance.add(p3);

        ActionParam p4 = new ActionParam("insurant-email", "insurer_mail", "строка@");
        p4.setDescr("E-mail страхуемого");
        p4.setTypeId("string");
        p4.setMinlength(8);
        instance.add(p4);

        ActionParam p5 = new ActionParam("insurant-doc-numberShort", "insurer_dn", "87654");
        p5.setDescr("Номер паспорта");
        p5.setTypeId("long");
        p5.setMinlength(6);
        instance.add(p5);

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);

        ErrorList check = instance.check();
        String errMsg = check.getErrorsMessage();
        Log.TEST.info(errMsg);

        Assertions.assertEquals(6, check.size());
    }

    @Test
    @SuppressWarnings({ "deprecation" })
    void testAddStructure() {
        ParamStructure instance = new ParamStructure();
        instance.add(new ActionParam("insurer-address-city", "insurer_address_city", "Краснодар"));
        instance.add(new ActionParam("insurer-address-street", "insurer_address_street", "Красная"));
        instance.add(new ActionParam("insurer-name", "insurer_name", "Иван"));
        instance.add(new ActionParam("insurer-surname", "insurer_surname", "Иванов"));
        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info("instance getValuesAsJson(): {}", asJson);

        ParamStructure addedInstance = new ParamStructure();
        addedInstance.add(new ActionParam("doc-type", "doc_type", "Паспорт"));
        addedInstance.add(new ActionParam("doc-number", "doc_number", "123456"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));
        asJson = addedInstance.getValuesAsJson(false);
        Log.TEST.info("addedInstance getValuesAsJson(): {}", asJson);

        String idn = instance.getValue("insurer-doc-number");
        Assertions.assertNull(idn);

        instance.addStructure("insurer", addedInstance);

        idn = instance.getValue("insurer-doc-number");
        Assertions.assertEquals("123456", idn);

        asJson = instance.getValuesAsJson(false);
        Log.TEST.info("result after adding getValuesAsJson(): {}", asJson);
    }

    @Test
    @SuppressWarnings({ "deprecation" })
    void testAddStructureToRoot() {
        ParamStructure instance = new ParamStructure();
        instance.add(new ActionParam("insurer-address-city", "insurer_address_city", "Краснодар"));
        instance.add(new ActionParam("insurer-address-street", "insurer_address_street", "Красная"));
        instance.add(new ActionParam("insurer-name", "insurer_name", "Иван"));
        instance.add(new ActionParam("insurer-surname", "insurer_surname", "Иванов"));

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info("instance getValuesAsJson(): {}", asJson);

        ParamStructure addedInstance = new ParamStructure();
        addedInstance.add(new ActionParam("doc-type", "doc_type", "Паспорт"));
        addedInstance.add(new ActionParam("doc-number", "doc_number", "123456"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));

        asJson = addedInstance.getValuesAsJson(false);
        Log.TEST.info("addedInstance getValuesAsJson(): {}", asJson);

        String idn = instance.getValue("doc-number");
        Assertions.assertNull(idn);

        instance.addStructure("", addedInstance);

        idn = instance.getValue("doc-number");
        Assertions.assertEquals("123456", idn);

        asJson = instance.getValuesAsJson(false);
        Log.TEST.info("result after adding getValuesAsJson(): {}", asJson);
    }

    @Test
    @SuppressWarnings({ "deprecation" })
    void testAddStructureToArrays() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Logger.getLogger(ParamStructureTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);

        ParamStructure addedInstance = new ParamStructure();
        addedInstance.add(new ActionParam("doc-type", "doc_type", "Паспорт"));
        addedInstance.add(new ActionParam("doc-number", "doc_number", "123456"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));
        asJson = addedInstance.getValuesAsJson(false);
        Log.TEST.info(asJson);

        instance.addStructure("insurer", addedInstance);

        Assertions.assertEquals(instance.getValue("insurer-address-city"), "Краснодар");
        Assertions.assertEquals(instance.getValue("insurer-phone"), "+7900-123-45-67");

        asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    @Test
    @SuppressWarnings({ "deprecation" })
    void testAddArraysToArrays() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        String y = "{\n"
                + "    'doc': [\n"
                + "        {\n"
                + "            'type': 'Паспорт',\n"
                + "            'number': '123456'\n"
                + "        },\n"
                + "        {\n"
                + "            'type': 'Права',\n"
                + "            'number': '987654'\n"
                + "        },\n"
                + "    ]\n"
                + "}";
        ParamStructure addedInstance = new ParamStructure();
        try {
            addedInstance.readJSON(y);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }
        addedInstance.add(new ActionParam("doc-series", "doc_series", "1234"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));
        instance.addStructure("insurer", addedInstance);

        Assertions.assertEquals(instance.getValue("insurer-doc-series"), "1234");
        Assertions.assertEquals(instance.getValue("insurer-phone"), "+7900-123-45-67");

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);
    }

    @Test
    @SuppressWarnings({ "deprecation" })
    void testRemove() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ],"
                + "    solo: {\n"
                + "        param1: 'value1',\n"
                + "        param2: value2\n"
                + "    }\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        String y = "{\n"
                + "    'doc': [\n"
                + "        {\n"
                + "            'type': 'Паспорт',\n"
                + "            'number': '123456'\n"
                + "        },\n"
                + "        {\n"
                + "            'type': 'Права',\n"
                + "            'number': '987654'\n"
                + "        },\n"
                + "    ]\n"
                + "}";
        ParamStructure addedInstance = new ParamStructure();
        try {
            addedInstance.readJSON(y);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }
        addedInstance.add(new ActionParam("doc-series", "doc_series", "1234"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));
        instance.addStructure("insurer", addedInstance);

        String nodeName = "solo-param1";
        Assertions.assertNotNull(instance.getValue(nodeName));
        Object removed = instance.remove(nodeName);
        Assertions.assertNull(instance.getValue(nodeName));
        Assertions.assertNotNull(removed);

        nodeName = "insurer-doc-number";
        Assertions.assertNotNull(instance.getValue(nodeName));
        removed = instance.remove(nodeName);
        Assertions.assertNull(instance.getValue(nodeName));
        Assertions.assertNotNull(removed);

        nodeName = "insurer-doc-nonexisting";
        Assertions.assertNull(instance.getValue(nodeName));
        removed = instance.remove(nodeName);
        Assertions.assertNull(instance.getValue(nodeName));
        List<Object> empty = new ArrayList<>();
        Assertions.assertEquals(empty, removed);

        nodeName = "insurer";
        Assertions.assertNull(instance.getValue(nodeName));
        removed = instance.remove(nodeName);
        Assertions.assertNull(instance.getValue(nodeName));
        Assertions.assertNotNull(removed);

        nodeName = "solo";
        Assertions.assertNull(instance.getValue(nodeName));
        removed = instance.remove(nodeName);
        Assertions.assertNull(instance.getValue(nodeName));
        Assertions.assertNotNull(removed);

        String asJson = instance.getValuesAsJson(false);
        Log.TEST.info(asJson);

    }

    @Test
    void testXml() {
        String x = "{\n"
                + "    'insurer': [\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Краснодар',\n"
                + "                'street': 'Красная'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Москва',\n"
                + "                'street': 'Тверская'\n"
                + "            }\n"
                + "        },\n"
                + "        {\n"
                + "            'address': {\n"
                + "                'city': 'Los Angeles',\n"
                + "                'street': 'Schwarzenegger avenue'\n"
                + "            }\n"
                + "        }\n"
                + "    ],"
                + "    solo: {\n"
                + "        param1: 'value1',\n"
                + "        param2: value2\n"
                + "    }\n"
                + "}";
        ParamStructure instance = new ParamStructure();
        try {
            instance.readJSON(x);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }

        String y = "{\n"
                + "    'doc': [\n"
                + "        {\n"
                + "            'type': 'Паспорт',\n"
                + "            'number': '123456'\n"
                + "        },\n"
                + "        {\n"
                + "            'type': 'Права',\n"
                + "            'number': '987654'\n"
                + "        },\n"
                + "    ]\n"
                + "}";
        ParamStructure addedInstance = new ParamStructure();
        try {
            addedInstance.readJSON(y);
        } catch (JSONException ex) {
            Log.TEST.error(null, ex);
        }
        addedInstance.add(new ActionParam("doc-series", "doc_series", "1234"));
        addedInstance.add(new ActionParam("phone", "phone", "+7900-123-45-67"));
        instance.addStructure("insurer", addedInstance);

        String asXml = instance.getValuesAsXml(false);
        Log.TEST.info(asXml);
    }

    @Test
    void testRemoveNonConfirmed() {
        ParamStructure incomeParam = new ParamStructure();
        ActionParam token = new ActionParam("token", "token", "user1111");
        ActionParam format = new ActionParam("format", "format", "xml");
        ActionParam constantName = new ActionParam("constantName", "constantName", "ucDictiNSCoverageTerritory");
        ActionParam includeArchived = new ActionParam("includeArchived", "includeArchived", "true");
        ActionParam modifiedSince = new ActionParam("modifiedSince", "modifiedSince", null);
        token.setConfirmed(false);
        format.setConfirmed(false);
        constantName.setConfirmed(true);
        includeArchived.setConfirmed(true);
        modifiedSince.setConfirmed(true);

        incomeParam.add(token);
        incomeParam.add(format);
        incomeParam.add(constantName);
        incomeParam.add(includeArchived);
        incomeParam.add(modifiedSince);

        Assertions.assertEquals(incomeParam.getStructure().size(), 5);

        incomeParam.removeNonconfirmed();

        Assertions.assertEquals(incomeParam.getStructure().size(), 3);
    }
}
