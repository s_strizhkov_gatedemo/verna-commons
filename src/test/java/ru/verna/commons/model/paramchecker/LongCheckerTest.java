package ru.verna.commons.model.paramchecker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.verna.commons.model.ActionParam;

public class LongCheckerTest {

    private static LongChecker longChecker;

    private static ActionParam param;

    @BeforeAll
    public static void setUpClass() {
        longChecker = new LongChecker();
        param = new ActionParam("test", "test", "0");
    }

    @Test
    public void checkContainChar() {
        param.setValue("123w");
        ErrorList errorList = longChecker.check(param);
        Assertions.assertEquals("значение должно содержать только цифры", errorList.get(0).getText());
    }

    @Test
    public void checkMaxLongError() {
        param.setValue("12345678901234567890");
        ErrorList errorList = longChecker.check(param);
        Assertions.assertEquals("превышена разрешенная длина значения", errorList.get(0).getText());
    }

    @Test
    public void checkNoError() {
        param.setValue("1234567890123456789");
        ErrorList errorList = longChecker.check(param);
        Assertions.assertTrue(errorList.isEmpty());
    }
}
