package ru.verna.commons;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class XmlUtilsTest {

    @Test
    public void getNodeValue() {
        String rawString =
                "<data>\n" +
                "  <result>\n" +
                "    <calcId>10645-МК-GF8VH</calcId>\n" +
                "    <isn>-68670</isn>\n" +
                "    <premium>3600</premium>\n" +
                "    <insurer>\n" +
                "      <isn>31839874</isn>\n" +
                "    </insurer>\n" +
                "  </result>\n" +
                "</data>";
        assertTrue("10645-МК-GF8VH".equalsIgnoreCase(XmlUtils.getNodeValue(rawString, "calcId")));
    }
}
