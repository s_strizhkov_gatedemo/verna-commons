package ru.verna.commons.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 */
public class RequestDescriptorTest {

    @Test
    public void withBody() {
        RequestDescriptor requestDescriptor = new RequestDescriptor();
        requestDescriptor.setSender("sen.der");
        requestDescriptor.setMethod("GET");
        requestDescriptor.setPath("/request/uri");
        Map<String, String> params = new TreeMap<>();
        params.put("param1", "value 1");
        params.put("param2", "value/2");
        params.put("param2", "value/3");// должен затереть предыдущий
        params.put("param4", "value:4");
        requestDescriptor.setParams(params);

        Map<String, String> headers = new TreeMap<>();
        headers.put("header 1", "value 1");
        headers.put("header 2", "value/2");
        headers.put("header 2", "value/3");// должен затереть предыдущий
        headers.put("header 4", "value:4");
        requestDescriptor.setHeaders(headers);

        requestDescriptor.setBody("body body body");

        String result = null;
        try {
            result = requestDescriptor.describe();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RequestDescriptorTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        String expected = "{\"sender\":\"sen.der\",\"method\":\"GET\",\"path\":\"/request/uri\",\"params\":{\"param1\":\"value 1\",\"param2\":\"value/3\",\"param4\":\"value:4\"},\"headers\":{\"header 1\":\"value 1\",\"header 2\":\"value/3\",\"header 4\":\"value:4\"},\"body\":\"body body body\"}";
        assertEquals(expected, result);
    }

    @Test
    public void withoutBody() {
        RequestDescriptor requestDescriptor = new RequestDescriptor();
        requestDescriptor.setSender("sen.der");
        requestDescriptor.setMethod("GET");
        requestDescriptor.setPath("/request/uri");
        Map<String, String> params = new TreeMap<>();
        params.put("param1", "value 1");
        params.put("param2", "value/2");
        params.put("param2", "value/3");// должен затереть предыдущий
        params.put("param4", "value:4");
        requestDescriptor.setParams(params);

        Map<String, String> headers = new TreeMap<>();
        headers.put("header 1", "value 1");
        headers.put("header 2", "value/2");
        headers.put("header 2", "value/3");// должен затереть предыдущий
        headers.put("header 4", "value:4");
        requestDescriptor.setHeaders(headers);

        String result = null;
        try {
            result = requestDescriptor.describe();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RequestDescriptorTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        String expected = "{\"sender\":\"sen.der\",\"method\":\"GET\",\"path\":\"/request/uri\",\"params\":{\"param1\":\"value 1\",\"param2\":\"value/3\",\"param4\":\"value:4\"},\"headers\":{\"header 1\":\"value 1\",\"header 2\":\"value/3\",\"header 4\":\"value:4\"},\"body\":null}";
        assertEquals(expected, result);
    }

    @Test
    public void emptyDescriptor() {
        RequestDescriptor requestDescriptor = new RequestDescriptor();

        String result = null;
        try {
            result = requestDescriptor.describe();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RequestDescriptorTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        String expected = "{\"sender\":null,\"method\":null,\"path\":null,\"params\":null,\"headers\":null,\"body\":null}";
        assertEquals(expected, result);
    }
}
