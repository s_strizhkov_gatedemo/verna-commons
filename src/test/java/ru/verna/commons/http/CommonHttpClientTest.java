package ru.verna.commons.http;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.verna.commons.log.Log;
import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CommonHttpClientTest {

    private static final String urlOverHttps = "https://test-gate.verna-group.ru/public/download?document_id=151487165&token=tokenofpublicuser";

    private static final String passwordKey = "password";

    @Test
    void sendHttpsGetFail() throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet getMethod = new HttpGet(urlOverHttps);
            getMethod.addHeader(Log.CALL_CONTEXT_HEADER, Log.TEST_CALL_CONTEXT);
            assertThrows(SSLHandshakeException.class, () -> httpClient.execute(getMethod));
        }
    }

    @Test
    void decodePasswordTest() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "12345678");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals("1234****", decodePassword.get(passwordKey));
    }

    @Test
    void decodePasswordTestShortPswd() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "я");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals("****", decodePassword.get(passwordKey));
    }

    @Test
    void decodePasswordTestNullPswd() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, null);
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertNull(decodePassword.get(passwordKey));
    }

    @Test
    void decodePasswordTestNullParams() {
        Map<String, String> params = null;
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertNull(decodePassword);
    }

    @Test
    void decodePasswordTest5() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "$%@");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals("$****", decodePassword.get(passwordKey));
    }

    @Test
    void decodePasswordTestAnotherParamsAreSame() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "$%@");
        String anotherKey = passwordKey + 2;
        params.put(anotherKey, "$%@");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals("$****", decodePassword.get(passwordKey));
        Assertions.assertEquals(2, params.size());
        Assertions.assertEquals("$%@", decodePassword.get(anotherKey));
    }

    @Test
    void decodePasswordTestAnotherParamsAreSame2() {
        Map<String, String> params = new HashMap<>();
        params.put("password2", "$%@");
        params.put("мама", "й345вусаып");
        params.put(passwordKey, "dfgh\nnlkf");
        params.put("мыла", "242");
        params.put("раму", "55567");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals(params.size(), decodePassword.size());
        Assertions.assertEquals("dfgh****", decodePassword.get(passwordKey));
        Assertions.assertEquals("$%@", decodePassword.get("password2"));
        Assertions.assertEquals("й345вусаып", decodePassword.get("мама"));
        Assertions.assertEquals("242", decodePassword.get("мыла"));
        Assertions.assertEquals("55567", decodePassword.get("раму"));
    }

    @Test
    void decodePasswordEnterInPswd() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "12\n45678");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals(params.size(), decodePassword.size());
        Assertions.assertEquals("12\n4****", decodePassword.get(passwordKey));
    }

    @Test
    void decodePasswordTabInPswd() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "1\t\n45678");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals(params.size(), decodePassword.size());
        Assertions.assertEquals("1\t\n4****", decodePassword.get(passwordKey));
    }

    @Test
    void decodePasswordTestEmptyPswd() {
        Map<String, String> params = new HashMap<>();
        params.put(passwordKey, "");
        Map<String, String> decodePassword = CommonHttpClient.copyMapWithDecodedPassword(params);
        Assertions.assertEquals("", decodePassword.get(passwordKey));
    }
}
